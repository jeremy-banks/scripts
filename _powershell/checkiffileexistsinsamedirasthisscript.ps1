#define filename we're looking for
$filename = "serverlist.txt"

#locate path script was executed from
$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
echo "scriptDir is $scriptDir"

#combine the path and filename to a single variable
$serverlistPath = $scriptDir + "\" + $filename
echo "serverlistPath is $serverlistPath"

#now perform the check if $filename is in the same path as this script when executed
if (-Not (Test-Path $serverlistPath)) { echo "serverlist.txt is not present in the same directory as this shell script, exiting script"; exit }
echo "serverlist.txt is present in the same directory as this shell script, proceeding"
