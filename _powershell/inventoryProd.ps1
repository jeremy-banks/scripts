#add powerCLI components
Add-PSSnapin VMware.VimAutomation.Core

#get date for unique filename
$curDateTime = Get-Date -format yyyyMMddhhmmss
$filename = "VMInventory" + $curDateTime + ".csv"
#echo "curDateTime is $curDateTime"
#echo "filename is $filename"



#locate path script was executed from
$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$serverlistPath = $scriptDir + "\serverlist.txt"
#echo "scriptDir is $scriptDir"
#echo "serverlistPath is $serverlistPath"

#check if serverlist.txt is in the same path as this script when executed
if (-Not (Test-Path $serverlistPath)) { echo "serverlist.txt is not present in the same directory as this shell script, exiting script"; exit }
#echo "serverlist.txt is present in the same directory as this shell script, proceeding"



#ask for username
$username = Read-Host "Enter your DOMAIN\USERNAME"
#echo "username is username"



#ask for password
$password = Read-Host -AsSecureString "Enter your PASSWORD"
#$userEnteredPW = Read-Host "Enter your PASSWORD" -AsSecureString
#$securedPassword = 
#echo "password is $password"



#ignore expired or invalid certs
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -confirm:$false
if (-Not ($?)) { echo "Unable to change configuration of PowerCLI to accept invalid certificates, exiting script"; exit	}

#ignore warnings about depricated commands
Set-PowerCLIConfiguration -DisplayDeprecationWarnings $false -Confirm:$false
if (-Not ($?)) { echo "Unable to change configuration of PowerCLI to stop warning about depticated commands being used, exiting script"; exit }


#test connection
#Connect-VIServer -server cld-and1-vctr01.and.hostedsolutions.com -user $username -password $password
#Connect-VIServer -server cld-and1-vctr01.and.hostedsolutions.com
#if (-Not ($?)) { echo "Unable to connect to server with your authentication"; Disconnect-VIServer -Force -confirm:$false; exit	}
#else {echo "Tested connection okay"}
#Disconnect-VIServer * -Force -confirm:$false
#if (-Not ($?)) { echo "Unable to connect to server with your authentication"; Disconnect-VIServer -Force -confirm:$false; exit	}
#else {echo "Tested disconnect okay"}


#make serverlist.txt into an array
$array = Get-Content $serverlistPath
foreach ($i in $array) {
#echo "$i"

#connect to server
Connect-VIServer -server $i -user $username -password ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($password)))

#get inventory
Get-VM | Select Name, Guest, PowerState, Version, Folder, ResourcePool, Description, Notes | Export-Csv -path "c:\$filename" -NoTypeInformation -Append

#disconnect from server
Disconnect-VIServer -server $i -confirm:$false
}


