#following scriptlet calls for username and password
#benefit of this scriptlet is that the password is stored secured, never visible on the command line, and encrypted even in memory

#ask for username
$username = Read-Host "Enter your DOMAIN\USERNAME"
#echo "username is username"

#ask for password
$password = Read-Host -AsSecureString "Enter your PASSWORD"
#echo "password is $password"

#example of calling the secured password
#examplecommand -server $server -user $username -password ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($password)))
