#!/bin/sh

#authored 2018/01/10 Jeremy Banks workjbanks@gmail.com

#following documentation here
#http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-ubuntu

#update apt
apt-get update -y

#install pre-reqs
apt-get install -y software-properties-common

#add ansible to apt repo
apt-add-repository -y ppa:ansible/ansible

#update apt again
apt-get update -y

#install ansible
apt-get install  -y ansible

#echo ansible version
ansible --version