#!/bin/bash

#12/12/2017 jbanks@zipwhip.com

#this script will backup and compress mysql database

#/tank/./mysql_bak.sh <backup_source> <temp_path> <backup_destination>
#~/./mysql_bak.sh /tank/percona-server/data /tmp /tank/bak

#crontab
#30 2 * * * /tank/./mysql_backup.sh /tank/percona-server /tmp /tank

#if /local_data isn't set up correctly this will abort
[ ! -d /local_data ] && { echo >&2 "/local_data is not a valid directory, aborting."; exit 1; }

#if your mysql creds are not stored in root ~/.my.cnf this will fail
[ ! -f /root/.my.cnf ] && { echo >&2 "/root/.my.cnf does not exist, aborting."; exit 1; }

#xtrabackup is needed for backup
command -v xtrabackup >/dev/null 2>&1 || { echo >&2 "xtrabackup is not installed, execute 'apt-get install -y percona-xtrabackup' and try again."; exit 1; }

#lz4 is needed for compression
command -v lz4 >/dev/null 2>&1 || { echo >&2 "lz4 is not installed, execute 'apt-get install -y liblz4-tool' and try again"; exit 1; }

#lowest possible CPU and IO
nice_lvl="/usr/bin/nice -n 19 /usr/bin/ionice -c2 -n7"

#for unique file and path names
YMDHMS="$(date +"%Y%m%d%I%M%S%3N")"

#backup source directory
bak_src=$1
nagios_mysql_backup_check="/local_data/nagios_mysql_backup_check"

#temporary data for storing files
tmp_path="${2}/$(hostname -a)-$YMDHMS"
tmp_path_data="${tmp_path}/data"
bakfile_filename="$(hostname -a)-$YMDHMS.tar.lz4"
bakfile_filename_fullpath="$tmp_path/$bakfile_filename"

#the final destination
bak_dest="${3}/bak/$(hostname -a)"

#make needed directories
#begin xtrabackup
#tar and compress backup
#move backup to destination
#prune backups older than 1 month
#set permission of backup
#touch nagios monitor file
#remove temp data
mkdir -p $tmp_path_data $bak_dest && \
$nice_lvl xtrabackup --backup --target-dir=$tmp_path_data && \
$nice_lvl tar cvf - $tmp_path_data | $nice_lvl lz4 - $bakfile_filename_fullpath && \
$nice_lvl mv "$bakfile_filename_fullpath" "$bak_dest" && \
cd $bak_dest && \
ls -tp | grep -v '/$' | tail -n +32 | xargs -d '\n' -r rm -- && \
chmod 400 $bak_dest/* && \
touch $nagios_mysql_backup_check
$nice_lvl rm -rf $tmp_path
