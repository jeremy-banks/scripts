#!/bin/bash

#jeremy banks workjbanks@gmail.com

#/bin/bash backup_rsync.sh <remote_hostname> <remote_host_ip> <key_path> <remote_path> <local_destination>
#/bin/bash ~/backup_rsync.sh 'sea-sandbox-01' '10.11.12.13' '/root/.ssh/id_rsa' '/locdrv/backup/localhost/' '/locdrv/backup/'

#rsync required
command -v rsync || { echo >&2 "rsync command not found" ; exit 127 ; }

remote_hostname=$1
remote_host_ip=$2
key_path=$3
remote_path=$4
local_destination=$5

#rsync -rv --chmod=400 -p -e "ssh -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" "root@www.xxx.yyy.zzz:/locdrv/backup/" "/locdrv/dataloc/"
rsync -rv --chmod=400 -p -e "ssh -i $key_path -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" "root@$remote_host_ip:$remote_path/" "$local_destination/$remote_hostname/"
