#!/bin/bash

#jeremy banks workjbanks@gmail.com

#/bin/bash backup_local.sh <name> <source> <destination>
#/bin/bash ~/backup_local.sh 'data' '/locdrv/data' '/locdrv/backups'

backup_name="$1"
src_path="$2"
dest_path="$3"
my_hostname="$(hostname)"
YMDHMS="$(date +"%Y%m%d%I%M%S%3N")"
new_backup_abspath="$dest_path/$my_hostname-$backup_name-$YMDHMS.tar.lz4"
last_backup_filename_wc="$dest_path/$my_hostname-$backup_name-*"
number_of_stored_backups=2

script_healthcheck () {
    #required commands
    command -V tar 1>/dev/null || exit 1
    command -V lz4 1>/dev/null || exit 1

    #hostname and date must resolve correctly
    hostname 1>/dev/null || { echo "hostname command is not exiting with status code 0" ; exit 1 ; }

    #source and destination directory must be reachable
    [ -d "$src_path" ] || { echo "cannot access '$src_path'" ; exit 1 ; }
    [ -d "$dest_path" ] || { echo "cannot access '$dest_path'" ; exit 1 ; }
}

take_new_backup () {
    #archive source and compress to destination
    tar cvf - "$src_path" | lz4 - "$new_backup_abspath" && \
    echo "created $new_backup_abspath"
}

delete_new_backup_if_no_size_change () {
    number_of_backups="$(ls $last_backup_filename_wc | wc -l)"
    #echo "number_of_backups"
    #echo "$number_of_backups"

    if [ "$number_of_backups" -eq "$number_of_backups" -a "$number_of_backups" -gt 1 ]; then
        new_backup_stat="$(stat -c '%s' "$new_backup_abspath")"
        #echo "new_backup_stat"
        #echo "$new_backup_stat"

        last_backup_abspath="$(ls -t $last_backup_filename_wc | sort -r | head -2 | tail -1)"
        #echo "last_backup_abspath"
        #echo "$last_backup_abspath"

        last_backup_stat="$(stat -c '%s' "$last_backup_abspath")"
        #echo "last_backup_stat"
        #echo "$last_backup_stat"

        [ "$last_backup_stat" -eq "$new_backup_stat" ] && \
            rm -f "$new_backup_abspath" && \
            echo "no change in file size since last backup" && \
            echo "deleted $new_backup_abspath"
    fi
}

truncate_backups () {
    number_of_backups="$(ls $last_backup_filename_wc | wc -l)"
    #echo "number_of_backups"
    #echo "$number_of_backups"

    if [ "$number_of_backups" -eq "$number_of_backups" -a "$number_of_backups" -gt "$number_of_stored_backups" ]; then
        echo "truncating backups..."

        backup_dir_contents="$(ls -t $last_backup_filename_wc | sort -r | tail -n +3 )"
        #echo "backup_dir_contents"
        #echo "$backup_dir_contents"

        for file in $backup_dir_contents; do
            rm -f "$file" && \
            echo "truncated $file"
        done

    fi
}

script_healthcheck
take_new_backup
delete_new_backup_if_no_size_change
truncate_backups
