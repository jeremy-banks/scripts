#!/bin/bash

#jeremy banks workjbanks@gmail.com

#/bin/bash backup_monthly.sh <backups_absolute_path>
#/bin/bash ~/backup_monthly.sh '/locdrv/backuparchive'

#this month
YM_this_month="$(date +"%Y%m")"
#echo -e "YM_this_month"
#echo -e "$YM_this_month"

#last month
YM_last_month="$(date '+%Y%m' --date '1 month ago')"
#YM_last_month=$YM_this_month
#echo -e "\nYM_last_month"
#echo -e "$YM_last_month"

#backups path
#readlink is used for absolute paths relative to root
backup_path="$(readlink -f $1)"
#echo -e "\nbackup_path"
#echo -e "$backup_path"

#fail if backup_path isn't valid directory
[[ ! -d "$backup_path" ]] && echo -e "error: target is not a directory, exiting" && exit 1

#get list of hosts
hosts="$(ls $backup_path)"
#echo -e "\nhosts"
#echo -e "$hosts"

#loop on each host
for host in $hosts
do
    echo -e "truncating monthly backups for host $host"

    #get all of last month's backups
    last_months_backups="$(ls $backup_path/$host/$host-*-$YM_last_month*.tar.lz4)"
    #echo -e "\nlast_months_backups"
    #echo -e "$last_months_backups"

    for backup in $last_months_backups
    do
        #echo -e "\nbackup"
        #echo -e "$backup"

        #fancy regex to filter everything but what is between the first two -
        backup_type=$(echo -e "$backup" | grep -o -P '(?<=-).*(?=-)')
        #echo -e "\nbackup_type"
        #echo -e "$backup_type"

        #we append it to a list
        backup_types+="$backup_type "
    done

    #echo -e "\nbackup_types"
    #echo -e "$backup_types"

    unique_backup_types=$(echo -e "$backup_types" | tr " " "\n" | sort | uniq | tr "\n" " ")
    #echo -e "\nunique_backup_types"
    #echo -e "$unique_backup_types"

    #if we don't unset then the variable will append through each looped host
    unset backup_types

    for type in $unique_backup_types
    do
        full_list_of_backups=$(ls $backup_path/$host/$host-$type-$YM_last_month*.tar.lz4)
        #echo -e "\nfull_list_of_backups"
        #echo -e "$full_list_of_backups"

        number_of_backups=$(ls $backup_path/$host/$host-$type-$YM_last_month*.tar.lz4 | wc -l)
        #echo -e "\nnumber_of_backups"
        #echo -e "$number_of_backups"

        if [ "$number_of_backups" -eq "$number_of_backups" -a "$number_of_backups" -gt 1 ]; then
            #list all backups of type, reverse sort, and don't show the newest entry
            backups_to_delete=$(ls $backup_path/$host/$host-$type-$YM_last_month*.tar.lz4 | sort -r | tail -n +2 )
            #echo -e "\nbackups_to_delete"
            #echo -e "$backups_to_delete"

            for bakup in $backups_to_delete
            do
                #verify what i'm deleting is a file
                [[ ! -f "$bakup" ]] && echo -e "error: delete target is not a file, exiting" && exit 1

                #remove and then delete
                rm "$bakup" && echo -e "deleted backup $bakup"
            done
        fi
    done

done
