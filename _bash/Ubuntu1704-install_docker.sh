#!/bin/sh

#authored 2017/10/14 Jeremy Banks workjbanks@gmail.com

#following documentation here
#https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/

#Uninstall old versions
apt-get remove -y docker docker-engine docker.io

apt-get update -y

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update -y

apt-get install -y docker-ce

docker --version