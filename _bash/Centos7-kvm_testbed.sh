#!/bin/sh

names=(seatest00,seatest01,seatest02)

for vm in "$names"
do
  virt-install --name=$vm --ram=1024 --vcpus=1 --cdrom=/var/CentOS-7-x86_64-Minimal-1708.iso --os-type=linux --os-variant=rhel7 --network bridge=virbr0 --graphics=spice --disk path=/external/vmvolumes/$vm.dsk,size=10
done
