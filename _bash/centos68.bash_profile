# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

alias tail='\tail -fn 75'
alias dir='\ls -laFh;pwd'
alias ping='\ping -c 6'
alias shit='sudo $(history -p !!)'
alias ansdir='\cd ~/platform-config/ansible/;\ls -laFh;pwd;git status'
alias wipeansdir='\cd ~;\rm -rf ~/platform-config;\git clone git@devtools.lab.hostedsolutions.com:platform-config;ansdir;\git config --global user.name \"Jeremy Banks\";\git config --global user.email jeremy.banks@tierpoint.com'
alias ansible-playbook='\ansible-playbook -vvv'
