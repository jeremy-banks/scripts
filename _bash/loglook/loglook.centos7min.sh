#!/bin/sh
#loglook.sh "logs/input/dir"

#set variables
logsDir=$1
niceLvl="/usr/bin/nice -n -20"
sstrings="./strings"
YMDHMS="$(date +"%Y%m%d%I%M%S%3N")"
ffilename=loglook$YMDHMS

#set current shell io to realtime to use more resources for loglook
ionice -c1 -p$$

#make output report file
$niceLvl touch $ffilename

#loop start
#for each file in logsDir
for Z in `ls -C -1 ${logsDir}/*`
do
	sstring=$Z
	echo $sstring
	string="/${sstring}/gi"
	#echo $string

	#for each line in strings file
	while read Y; do
		ssstring=$Y
		#echo $ssstring

		rresult="$(grep -rnw $logsDir -e "$ssstring")"
		#echo $rresult

		#this part uses double pipe to ensure that no duplicate results are recorded in the report
		#if the first command succeed the second will never be executed
		$niceLvl grep -q -F "$rresult" $ffilename || $niceLvl echo "$rresult" >> $ffilename
	done <$sstrings
done
$niceLvl sort -o $ffilename $ffilename