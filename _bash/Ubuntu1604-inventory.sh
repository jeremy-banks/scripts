#!/bin/bash

#2018/01/12 jbanks@zipwhip.com

#./inv.sh ~ /tmp

#variables
tmp_path_provided=$1
final_path=$2
final_compressed_path=$final_path/inventory.tar.gz
YMDHMS="$(date +"%Y%m%d%I%M%S%3N")"
tmp_path_used=$tmp_path_provided/$YMDHMS

#make directory
mkdir -p $final_path $tmp_path_used

#bios version
echo -e "BIOSver" > $tmp_path_used/system_version
dmidecode -s bios-version 2>/dev/null >> $tmp_path_used/system_version

#os version
echo -e "\n\nOS ver" >> $tmp_path_used/system_version
lsb_release -ds 2>/dev/null >> $tmp_path_used/system_version

#kernel version
echo -e "\n\nKernel ver" >> $tmp_path_used/system_version
uname -r 2>/dev/null >> $tmp_path_used/system_version

#which packages are installed
dpkg-query -l 2>/dev/null > $tmp_path_used/packages_installed

#which services are running
service --status-all 2>/dev/null |& grep + > $tmp_path_used/services_running

#which processes are running
ps eu 2>/dev/null > $tmp_path_used/processes_running

#any VPN tunnels up?
ipsec whack --trafficstatus 2>/dev/null  > $tmp_path_used/ipsec_trafficstatus

#what is in root cron
crontab -u root -l 2>/dev/null > $tmp_path_used/crontab

#what is in fstab
cat /etc/fstab 2>/dev/null > $tmp_path_used/fstab

#get list of docker containers
containers_running="$(docker ps -aq 2>/dev/null)"

#loop through list of containers
for c in $containers_running
do
	#get info from inspect
	docker_inspect="$(docker inspect $c)"
	
	#grep for the container name
	grep -m 1 "Name" <<< "$docker_inspect" >> $tmp_path_used/containers_running
	
	#grep for the container id
	grep -m 1 "Id" <<< "$docker_inspect" >> $tmp_path_used/containers_running
	
	#awk for the mounts
	awk "/Mounts/,/]/" <<< "$docker_inspect" >> $tmp_path_used/containers_running
	
	#add some padding to the end of the file
	echo -e "\n\n\n" >> $tmp_path_used/containers_running
done

#largest directories
du -ax / \
	--exclude "/media" \
	--exclude "/mnt" \
	--exclude "/share" \
	--exclude "/shared_data" \
	--exclude "/shared_cfg" \
	2>/dev/null |\
	sort -nr |\
	head -100 \
	> $tmp_path_used/largest_dirs

#largest files
find / \
	\( -wholename "/media/*" -prune \) -o \
	\( -wholename "/mnt/*" -prune \) -o \
	\( -wholename "/share/*" -prune \) -o \
	\( -wholename "/shared_data/*" -prune \) -o \
	\( -wholename "/shared_cfg/*" -prune \) -o \
	-type f -printf "%s %p\n" \
	2>/dev/null |\
	sort -nr |\
	head -100 \
	> $tmp_path_used/largest_files

#grep all files for potential private keys
grep \
	-Ilr \
	--exclude-dir=media \
	--exclude-dir=mnt \
	--exclude-dir=share \
	--exclude-dir=shared_data \
	--exclude-dir=shared_cfg \
	--exclude=*.py \
	--exclude=*.sh \
	--exclude=.viminfo \
	-e "-----BEGIN PRIVATE" \
	-e "-----BEGIN RSA" \
	-e "-----BEGIN EC" \
	/ \
	2>/dev/null \
	> $tmp_path_used/private_keys_found

#grep all files for potential certificates
some_magic()
{
while read p; do
	#if cert expired or will expire within 1 year
	if ! openssl x509 -checkend 31557600 -noout -in $p 2>/dev/null
	then
		openssl x509 -enddate -noout -in $p 2>/dev/null >> $tmp_path_used/expired_certs && \
		echo "$p" >> $tmp_path_used/expired_certs && \
		echo -e "\n\n" >> $tmp_path_used/expired_certs
	fi
done <$tmp_path_used/certificates_found
}

grep \
	-Ilr \
	--exclude-dir=media \
	--exclude-dir=mnt \
	--exclude-dir=share \
	--exclude-dir=shared_data \
	--exclude-dir=shared_cfg \
	--exclude=*.py \
	--exclude=*.sh \
	--exclude=.viminfo \
	-e "-----BEGIN CERTIFICATE" \
	/ \
	2>/dev/null \
	> $tmp_path_used/certificates_found ; \
	some_magic

#tar, compress, move file to destination for acquisition, delete temp files
rm -f "$final_compressed_path" && \
	cd "$tmp_path_used" && \
	tar -zcvf "$final_compressed_path" . && \
	cd / && \
	rm -rf "$tmp_path_used"
