arr = []

for x in range(1,101):
    arr.append(x)

#print(arr)

for y in arr:
    a = y % 3
    #print(a)

    b = y % 5
    #print(b)

    if a == 0 and b == 0: #fizzbuzz
        print(str(y) + " fizzbuzz")
    elif a == 0 and b != 0: #fizz
        print(str(y) + " fizz")
    elif a != 0 and b == 0: #buzz
        print(str(y) + " buzz")
    else:
        print(y)
