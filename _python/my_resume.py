#workjbanks@gmail.com
#python jeremy_banks_resume.py | less

candidates = {
    'jeremy': {
        'header': { 'full_name': 'Jeremy Banks', 'phone_number': '(360) 454-8384', 'email_address': 'workjbanks@gmail.com' },
        'urls': [ 'linkedin.com/in/hire-jeremy-banks', 'bitbucket.org/jeremy-banks', 'pastebin.com/blah' ],
        'skills': [
            ('Python', 1),
            ('Linux', 7),
            ('Unix', 5),
            ('Jenkins', 1),
            ('SQL', 3),
            ('C#', 1),
            ('Docker', 1),
            ('AWS', 1),
            ('Azure', 1),
            ('Nagios', 1),
            ('Bash Scripting', 3),
            ('Ansible', 2),
            ('Git', 2),
            ('Packer', 1),
            ('Terraform', 1),
            ('Troubleshooting', 10)
        ],
        'experience': {
            'Zipwhip': { 'title': 'Systems Engineer', 'type': 'Fulltime',  'joined': 'Oct 2017', 'left': 'Mar 2018', 'duration': '6m', 'duties': [
                'Provide newly hired Director with analysis of existing services and products running on development and production environment',
                'Advise team on selection of provisioning, configuration management, and CI/CD tools for team',
                'Build, tune, and document rollout for production XtraDB cluster including Nagios monitoring and alerts for cluster health',
                'Tuning of existing Rancher cluster nodes and incorporated Ansible to build cluster nodes from code'
                ]
            },
            'TierPoint': { 'title': 'Systems Engineer', 'type': 'Contract',  'joined': 'Oct 2016', 'left': 'Jan 2017', 'duration': '4m', 'duties': [
                'Installs, maintains and upgrades the systems and technologies that constitute the internal applications (Platforms) used in the support of Datacenter and Hosting customers',
                'Responsible for the operations and upkeep of the Platform related systems',
                'Work closely with the engineering and development teams on new implementations as well as maintaining the current environments',
                'Act as a technical escalation point within Engineering for Platform specific issues',
                'Responsible for implementing and guaranteeing process and systems are in place to deliver highly available systems and applications used within the platform ecosystem',
                'Creates documentation for supported environments, processes, technical training, and support procedures',
                'Provides training and technical leadership to Tier I personnel regarding triage, escalation and notification',
                'Provide additional support when necessary for lab infrastructure related to the support of test Platform systems',
                'Drives efficient and effective resolution of issues'
                ]
            },
            'Cisco': { 'title': 'Customer Support Engineer', 'type': 'Contract', 'joined': 'Apr 2015', 'left': 'Sep 2016', 'duration': '1y6m', 'duties': [
                'Support Tidal Enterprise Scheduler 5.x and 6.x, including Client Manager, Java Client, Windows and Unix Agents, Transporter, and all Adapters including SAP and Informatica',
                'Respond to production emergency situations for external and internal Cisco customers, provide analysis and root cause of the issue, and restore service or escalate to development as needed',
                'Research complex job structures, provide feedback regarding job logic, and advise on more efficient production paths utilizing advanced Tidal features such as Job Classes, Variables, and Security Policies',
                'Twice won 1st place competing with region in submission of technical articles, script creation, and sales lead generation'
                ]
            },
            'LCMS Plus': { 'title': 'System Administrator', 'type': 'Fulltime', 'joined': 'Jan 2015', 'left': 'Mar 2015', 'duration': '3m', 'duties': [
                'Setup of virtual servers on Rackspace, secure certs and related elements required to establish new client systems',
                'Intrusion detection and maintaining system security',
                'System upgrades and patch management',
                'Staying ahead of OS updates, browser versions and new offerings from hosting providers that might enhance our service to our clients',
                'Working with clients to integrate LCMS+ with their internal authentication (LDAP, Shibboleth) and to help them manage any internal hosting environment challenges'
                ]
            },
            'IBM / Lenovo': { 'title': 'System Administrator', 'type': 'Contract', 'joined': 'Jun 2014', 'left': 'Dec 2014', 'duration': '7m', 'duties': [
                'Assembled IBM System X server blade hardware according to testing schedule parameters',
                'Installed various operating systems including Windows Server 2008 and 2012 R2, Red Hat Enterprise Linux, SUSE Linux Enterprise Server (SLES), and VMware 5.5 ESXi with vSphere',
                'Set up virtual guest environments on host operating systems, including Xen3, KVM, Hyper-V, and VMware',
                'Conducted CPU load and Memory stress testing of hardware and software using MPx, AVX, and PScan to identify failures',
                'Observed and recorded results of tests using UEFI System Logs, operating system logs, IBM Chassis Management Module, and Integrated Management Module',
                'Identified and reported defects in IBM Rational ClearQuest, worked with team of developers to analyse defects, and test updated firmware to confirm defect resolution',
                'Issued product testing training to new team members'
                ]
            },
            'Becton, Dickinson and Company': { 'title': 'Systems Engineer', 'type': 'Fulltime', 'joined': 'May 2007', 'left': 'Jun 2014', 'duration': '7y2m', 'duties': [
                'Provided support for Salesforce, Excel, Word, Adobe, Java, AutoCAD, Minitab, Iron Mountain Connected Backup, Snag-It, and inter-office virtual conference equipment',
                'Delivered migrations from Windows XP to Windows 7, and Lotus Notes to Office 2010',
                'Administrated SCCM package deployment, including pilot testing for quarterly build releases and major software rollouts',
                'Led SCCM compliance team for three sites, also core team member of software compliance for all workstations in Canada, United States, and North and South Latin America',
                'Coached and trained technicians across the country on efficient compliance resolution tactics',
                'Directed document disaster recovery plan for Legacy Solaris Unix instrument production server and workstations, reduced work cell downtime to a 1 hour maximum in the event of any system failure by providing work cell clones stored off site',
                'Managed NIS+ Solaris Unix account administration for instrument support team, assigned IP addresses for medical instruments to be manufactured',
                'Utilized various ticketing systems including Peregrin Service Center, Remedy, and in-house solutions',
                'Core team member of BYOD support, account creation, troubleshooting, and separation of smart phones, including Blackberry, Android, Windows, iPad, and iPhone',
                'Leveraged Blackberry Enterprise Server and ActiveSync to provide Mobile Device Management support to customers across the globe',
                'Authored and audited support documentation for company\'s global help desk scripts',
                'Recorded video presentations and wrote training documentation to feature new available technologies, including multi-factor authentication, Office 365, Mobile Iron, and ActiveSync',
                'Advised manufacturing team regarding issues with Legacy Solaris Unix instrument production server'
                ]
            },
            'Volt': { 'title': 'Support Specialist', 'type': 'Contract', 'joined': 'Sep 2006', 'left': 'May 2007', 'duration': '9m', 'duties': [
                'Installation and repair of Windows on laptops and workstations for internal associates',
                'Acted as on-site technician for remote Redmond WA site',
                'Supported Lotus Notes 6.x and 8.x including various database attachments, Office 2003, McAfee antivirus, and in-house applications for sales and production inventory',
                'Aided in hardware support for Unix Solaris production server, workstations, and network issues'
                ]
            },
            'Fry\'s Electronics': { 'title': 'Management Trainee', 'type': 'Fulltime', 'joined': 'Aug 2005', 'left': 'Jun 2006', 'duration': '11m', 'duties': [
                'Regularly excelled in submission of store credit card and extended warranty sales',
                'Won 1st place in Sales For 2005 for Electronic Components department',
                'Trained new hires on best practices'
                ]
            }
        }
    }
}

import subprocess
rows, columns = subprocess.check_output(['stty', 'size']).split()
middle = int(columns) / 2
tab = "   "
button = " - "

def print_column(cols, padded, str1, str2, str3):
    if cols != 2 and cols != 3:#i have only coded for 2 or 3 columns
        print("only used for two or three columns")
        return True

    if padded == True:
        str1 = tab + str1

    beg_str_length = len(str1)
    mid_str_length = len(str2)
    end_str_length = len(str3)

    spacer_length = int(columns) - mid_str_length - end_str_length / (cols - 1)

    print(str1.ljust(spacer_length) + str2 + str3)
 
def print_header(header_dict, urls_list):
    #first line gives full name and phone number
    print_column(2, False, header_dict['full_name'], header_dict['phone_number'], "")

    y = 0
    for url in urls_list:
        if y == 0:#second line gives email address
            print_column(2, False, tab + url, header_dict['email_address'], "")
            y += 1
        else:#rest of lines print urls
            print(tab + url)
    
    print("")

def get_key(item):
    return item[1]

def print_skills(dict):
    print("SKILLS")

    sorted_list = []
    for skill in sorted(sorted(dict), key=get_key, reverse=True):#sorts by years of proficiency, then by name of skill
        sorted_list.append(str(skill[0]) + " " + str(skill[1]) + "yrs")#append the skill to our sorted list

    count = len(sorted_list)

    print(count)


    
    print (sorted_list)

    print("")

def print_experience(dict):
    print("EXPERIENCE")
    for role in dict:
        print(role)
        print_column(2, False, tab + dict[role]['type'] + ", " + dict[role]['title'], tab + dict[role]['joined'] + " - " + dict[role]['left'] + ", " + dict[role]['duration'], "")
        for duty in dict[role]['duties']:
            import textwrap
            msg = textwrap.TextWrapper(initial_indent=button, width=int(columns), subsequent_indent=tab)
            print(msg.fill(duty))
        print("")

def print_resume(candidate_dict):
    for candidate in candidate_dict:
        #print_header(candidate_dict[candidate]['header'], candidate_dict[candidate]['urls'],)
        print_skills(candidate_dict[candidate]['skills'])
        #print_experience(candidate_dict[candidate]['experience'])

print_resume(candidates)
