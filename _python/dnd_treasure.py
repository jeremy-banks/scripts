#workjbanks@gmail.com

import random
import sys
from optparse import OptionParser

cp = "cp"
sp = "sp"
ep = "ep"
gp = "gp"
pp = "pp"

parser = OptionParser()
parser.add_option("-t", "--type", dest="type", help="ind|hoard", default="ind")
parser.add_option("-c", "--cr", dest="cr", help="[0-17]", default="1")
parser.add_option("-b", "--books", dest="books", help="feature content from an entire book\n\tall|phb,eepc,mtf,scag,toa,vgm,wge,xge", default="phb")
parser.add_option("-o", "--options", dest="options", help="specific options to include\n\t all|ren_firearms,modern_firearms,futuristic_firearms", default="none")

(options, args) = parser.parse_args()

phb = False
eepc = False
mtf = False
scag = False
toa = False
vgm = False
wge = False
xge = False
if "phb" in options.books or "all" in options.books:
    phb = True
if "dmg" in options.books or "all" in options.books:
    dmg = True
if "eepc" in options.books or "all" in options.books:
    eepc = True
if "mtf" in options.books or "all" in options.books:
    mtf = True
if "scag" in options.books or "all" in options.books:
    scag = True
if "toa" in options.books or "all" in options.books:
    toa = True
if "vgm" in options.books or "all" in options.books:
    vgm = True
if "wge" in options.books or "all" in options.books:
    wge = True
if "xge" in options.books or "all" in options.books:
    xge = True

ren_firearms = False
modern_firearms = False
futuristic_firearms = False
if "ren_firearms" in options.options or 'all' in options.options:
    ren_firearms = True
if "modern_firearms" in options.options or 'all' in options.options:
    modern_firearms = True
if "futuristic_firearms" in options.options or 'all' in options.options:
    futuristic_firearms = True

def ten_gp_gemstones(num, die):
    value = "10gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,12) #1d12

        if roll == 1:
            treasure_final_result.append(value + " " + "Azurite (opaque mottled deep blue)")
        elif roll == 2:
            desc_roll = random.randint(1,4)
            if desc_roll == 1:
                desc = "translucent striped brown"
            elif desc_roll == 2:
                desc = "blue"
            elif desc_roll == 3:
                desc = "white"
            elif desc_roll == 4:
                desc = "red"
            treasure_final_result.append(value + " " + "Banded agate (" + desc + ")")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Blue quartz (transparent pale blue)")
        elif roll == 4:
            desc_roll = random.randint(1,5)
            if desc_roll == 1:
                desc = "gray"
            elif desc_roll == 2:
                desc = "white"
            elif desc_roll == 3:
                desc = "brown"
            elif desc_roll == 4:
                desc = "blue"
            elif desc_roll == 5:
                desc = "green"
            treasure_final_result.append(value + " " + "Eye agate (translucent circles of " + desc + ")")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Hematite (opaque gray-black)")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Lapis lazuli (opaque light and dark blue with yellow flecks)")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Malachite (opaque striated light and dark green)")
        elif roll == 8:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "translucent pink"
            elif desc_roll == 2:
                desc = "yellow-white with mossy gray markings"
            elif desc_roll == 3:
                desc = "yellow-white with mossy green markings"
            treasure_final_result.append(value + " " + "Moss agate (" + desc + ")")
        elif roll == 9:
            treasure_final_result.append(value + " " + "Obsidian (opaque black)")
        elif roll == 10:
            treasure_final_result.append(value + " " + "Rhodochrosite (opaque light pink)")
        elif roll == 11:
            treasure_final_result.append(value + " " + "Tiger eye (translucent brown with golden center)")
        elif roll == 12:
            treasure_final_result.append(value + " " + "Turquoise (opaque light blue-green")
    
def fifty_gp_gemstones(num, die):
    value = "50gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,12) #1d12

        if roll == 1:
            treasure_final_result.append(value + " " + "Bloodstone (opaque dark gray with red flecks)")
        elif roll == 2:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "opaque orange"
            elif desc_roll == 2:
                desc = "red-brown"
            treasure_final_result.append(value + " " + "Carnelian (" + desc + ")")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Chalcedony (opaque white)")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Chrysoprase (translucent green)")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Citrine (transparent pale yellow-brown)")
        elif roll == 6:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "opaque blue"
            elif desc_roll == 2:
                desc = "black"
            elif desc_roll == 3:
                desc = "brown"
            treasure_final_result.append(value + " " + "Jasper (" + desc + ")")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Moonstone (translucent white with pale blue glow)")
        elif roll == 8:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "opaque bands of black and white"
            elif desc_roll == 2:
                desc = "pure black"
            elif desc_roll == 3:
                desc = "white"
            treasure_final_result.append(value + " " + "Onyx (" + desc + ")")
        elif roll == 9:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "transparent white"
            elif desc_roll == 2:
                desc = "smoky gray"
            elif desc_roll == 3:
                desc = "yellow"
            treasure_final_result.append(value + " " + "Quartz (" + desc + ")")
        elif roll == 10:
            treasure_final_result.append(value + " " + "Sardonyx (opaque bands of red and white)")
        elif roll == 11:
            treasure_final_result.append(value + " " + "Star rose quartz (translucent rosy stone with white star-shaped center)")
        elif roll == 12:
            treasure_final_result.append(value + " " + "Zircon (transparent pale blue-green)")

def onehundred_gp_gemstones(num, die):
    value = "100gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,10) #1d10

        if roll == 1:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "transparent watery gold"
            elif desc_roll == 2:
                desc = "rich gold"
            treasure_final_result.append(value + " " + "Amber (" + desc + ")")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Amethyst (transparent deep purple)")
        elif roll == 3:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "transparent yellow-green"
            elif desc_roll == 2:
                desc = "pale green"
            treasure_final_result.append(value + " " + "Chrysoberyl (" + desc + ")")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Coral (opaque crimson)")
        elif roll == 5:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "transparent red"
            elif desc_roll == 2:
                desc = "brown-green"
            elif desc_roll == 3:
                desc = "violet"
            treasure_final_result.append(value + " " + "Garnet (" + desc + ")")
        elif roll == 6:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "translucent light green"
            elif desc_roll == 2:
                desc = "deep green"
            elif desc_roll == 3:
                desc = "white"
            treasure_final_result.append(value + " " + "Jade (" + desc + ")")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Jet (opaque deep black)")
        elif roll == 8:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "opaque lustrous whitee"
            elif desc_roll == 2:
                desc = "yellow"
            elif desc_roll == 3:
                desc = "pink"
            treasure_final_result.append(value + " " + "Pearl (" + desc + ")")
        elif roll == 9:
            desc_roll = random.randint(1,3)
            if desc_roll == 1:
                desc = "transparent red"
            elif desc_roll == 2:
                desc = "red-brow"
            elif desc_roll == 3:
                desc = "deep gree"
            treasure_final_result.append(value + " " + "Spinel (" + desc + ")")
        elif roll == 10:
            desc_roll = random.randint(1,4)
            if desc_roll == 1:
                desc = "transparent pale green"
            elif desc_roll == 2:
                desc = "blue"
            elif desc_roll == 3:
                desc = "brown"
            elif desc_roll == 4:
                desc = "red"
            treasure_final_result.append(value + " " + "Tourmaline (" + desc + ")")

def fivehundred_gp_gemstones(num, die):
    value = "500gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,6) #1d6

        if roll == 1:
            treasure_final_result.append(value + " " + "Alexandrite (transparent dark green)")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Aquamarine (transparent pale blue-green)")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Black pearl (opaque pure black)")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Blue spinel (transparent deep blue)")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Peridot (transparent rich olive green)")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Topaz (transparent golden yellow)")

def onethousand_gp_gemstones(num, die):
    value = "1,000gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,8) #1d8

        if roll == 1:
            treasure_final_result.append(value + " " + "Black opal (translucent dark green with black mottling and golden flecks)")
        elif roll == 2:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "transparent blue-white"
            elif desc_roll == 2:
                desc = "medium blue"
            treasure_final_result.append(value + " " + "Blue sapphire (" + desc + ")")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Emerald (transparent deep bright green)")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Fire opal (translucent fiery red)")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Opal (translucent pale blue with green and golden mottling)")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Star ruby (translucent ruby with white star-shaped center)")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Star sapphire (translucent blue sapphire with white star-shaped center)")
        elif roll == 8:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "transparent fiery yellow"
            elif desc_roll == 2:
                desc = "yellow-green"
            treasure_final_result.append(value + " " + "Yellow sapphire (" + desc + ")")

def fivethousand_gp_gemstones(num, die):
    value = "5,000gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,4) #1d4

        if roll == 1:
            treasure_final_result.append(value + " " + "Black sapphire (translucent lustrous black with glowing highlights)")
        elif roll == 2:
            desc_roll = random.randint(1,5)
            if desc_roll == 1:
                desc = "blue-white"
            elif desc_roll == 2:
                desc = "canary"
            elif desc_roll == 3:
                desc = "pink"
            elif desc_roll == 4:
                desc = "brown"
            elif desc_roll == 5:
                desc = "blue"
            treasure_final_result.append(value + " " + "Diamond (transparent " + desc + ")")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Jacinth (transparent fiery orange)")
        elif roll == 4:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "transparent clear crimson"
            elif desc_roll == 2:
                desc = "deep crimson"
            treasure_final_result.append(value + " " + "Ruby (" + desc + ")")

def twentyfive_gp_art_objects(num, die):
    value = "25gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,10) #1d10

        if roll == 1:
            treasure_final_result.append(value + " " + "Silver ewer")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Carved bone statuette")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Small gold bracelet")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Cloth-of-gold vestments")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Black velvet mask stitched with silver thread")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Copper chalice with silver filigree")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Pair of engraved bone dice")
        elif roll == 8:
            treasure_final_result.append(value + " " + "Small mirror set in a painted wooden frame")
        elif roll == 9:
            treasure_final_result.append(value + " " + "Embroidered silk handkerchief")
        elif roll == 10:
            treasure_final_result.append(value + " " + "Gold locket with a painted portrait inside")

def twohundredfifty_gp_art_objects(num, die):
    value = "250gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,10) #1d10

        if roll == 1:
            treasure_final_result.append(value + " " + "Gold ring set with bloodstones")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Carved ivory statuette")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Large gold bracelet")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Silver necklace with a gemstone pendant")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Bronze crown")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Silk robe with gold embroidery")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Large well-made tapestry")
        elif roll == 8:
            treasure_final_result.append(value + " " + "Brass mug with jade inlay")
        elif roll == 9:
            treasure_final_result.append(value + " " + "Box of turquoise animal figurines")
        elif roll == 10:
            treasure_final_result.append(value + " " + "Gold bird cage with electrum filigree")

def sevenhundredfifty_gp_art_objects(num, die):
    value = "750gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,10) #1d10

        if roll == 1:
            treasure_final_result.append(value + " " + "Silver chalice set with moonstones")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Silver-plated steellongsword with jet set in hilt")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Carved harp of exotic wood with ivory inlay and zircon gems")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Small gold idol")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Gold dragon comb set with red garnets as eyes")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Bottle stopper cork embossed with gold leaf and set with amethysts")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Ceremonial electrum dagger with a black pearl in the pommel")
        elif roll == 8:
            treasure_final_result.append(value + " " + "Silver and gold brooch")
        elif roll == 9:
            treasure_final_result.append(value + " " + "Obsidian statuette with gold fittings and inlay")
        elif roll == 10:
            treasure_final_result.append(value + " " + "Painted gold war mask")

def twothousandfivehundred_gp_art_objects(num, die):
    value = "2,500gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,10) #1d10

        if roll == 1:
            treasure_final_result.append(value + " " + "Fine gold chain set with a fire opal")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Old masterpiece painting")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Embroidered silk and velvet mantle set with numerous moonstones")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Platinum bracelet set with a sapphire")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Embroidered glove set with jewel chips")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Jeweled anklet")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Gold music box")
        elif roll == 8:
            treasure_final_result.append(value + " " + "Gold circlet set with four aquamarines")
        elif roll == 9:
            treasure_final_result.append(value + " " + "Eye patch with a mock eye set in blue sapphire and moonstone")
        elif roll == 10:
            treasure_final_result.append(value + " " + "A necklace string of small pink pearls")

def seventhousandfivehundred_gp_art_objects(num, die):
    value = "7,500gp"
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,8) #1d8

        if roll == 1:
            treasure_final_result.append(value + " " + "Jeweled gold crown")
        elif roll == 2:
            treasure_final_result.append(value + " " + "Jeweled platinum ring")
        elif roll == 3:
            treasure_final_result.append(value + " " + "Small gold statuette set with rubies")
        elif roll == 4:
            treasure_final_result.append(value + " " + "Gold cup set with emeralds")
        elif roll == 5:
            treasure_final_result.append(value + " " + "Gold jewelry box with platinum filigree")
        elif roll == 6:
            treasure_final_result.append(value + " " + "Painted gold child's sarcophagus")
        elif roll == 7:
            treasure_final_result.append(value + " " + "Jade game board with solid gold playing pieces")
        elif roll == 8:
            treasure_final_result.append(value + " " + "Bejeweled ivory drinking horn with gold filigree")

def random_spell(lvl):
    if lvl == 0:#cantrips
        phb_list = [ "Acid Splash", "Blade Ward", "Chill Touch", "Dancing Lights", "Druidcraft", "Eldritch Blast", "Fire Bolt", "Friends", "Guidance", "Light", "Mage Hand", "Mending", "Message", "Minor Illusion", "Poison Spray", "Prestidigitation", "Produce Flame", "Ray of Frost", "Resistance", "Sacred Flame", "Shillelagh", "Shocking Grasp", "Spare the Dying", "Thaumaturgy", "Thorn Whip", "True Strike", "Vicious Mockery" ]
        ee_list = [ "Control Flames", "Create Bonfire", "Frostbite", "Gust", "Magic Stone", "Mold Earth", "Shape Water", "Thunderclap" ]
        scag_list = [ "Booming Blade", "Green-Flame Blade", "Lightning Lure", "Sword Burst" ]
        xge_list = [ "Control Flames", "Create Bonfire", "Frostbite", "Gust", "Infestation", "Magic Stone", "Mold Earth", "Primal Savagery", "Thunderclap", "Toll the Dead", "Word of Radiance" ]
    elif lvl == 1:
        phb_list = [ "Alarm", "Animal Friendship", "Armor of Agathys", "Arms of Hadar", "Bane", "Bless", "Burning Hands", "Charm Person", "Chromatic Orb", "Color Spray", "Command", "Compelled Duel", "Comprehend Languages", "Create or Destroy Water", "Cure Wounds", "Detect Evil and Good", "Detect Magic", "Detect Poison and Disease", "Disguise Self", "Dissonant Whispers", "Divine Favor", "Ensnaring Strike", "Entangle", "Expeditious Retreat", "Faerie Fire", "False Life", "Feather Fall", "Find Familiar", "Fog Cloud", "Goodberry", "Grease", "Guiding Bolt", "Hail of Thorns", "Healing Word", "Hellish Rebuke", "Heroism", "Hex", "Hunter's Mark", "Identify", "Illusory Script", "Inflict Wounds", "Jump", "Longstrider", "Mage Armor", "Magic Missile", "Protection from Evil and Good", "Purify Food and Drink", "Ray of Sickness", "Sanctuary", "Searing Smite", "Shield", "Shield of Faith", "Silent Image", "Sleep", "Speak with Animals", "Tasha's Hideous Laughter", "Tenser's Floating Disk", "Thunderous Smite", "Thunderwave", "Unseen Servant", "Witch Bolt", "Wrathful Smite" ]
        ee_list = [ "Absorb Elements", "Beast Bond", "Catapult", "Earth Tremor", "Ice Knife" ]
        scag_list = []
        xge_list = [ "Absorb Elements", "Beast Bond", "Catapult", "Cause Fear", "Ceremony", "Chaos Bolt", "Earth Tremor", "Ice Knife", "Snare", "Zephyr Strike" ]
    elif lvl == 2:
        phb_list = [ "Aid", "Alter Self", "Animal Messenger", "Arcane Lock", "Augury", "Barkskin", "Beast Sense", "Blindness/Deafness", "Blur", "Branding Smite", "Calm Emotions", "Cloud of Daggers", "Continual Flame", "Cordon of Arrows", "Crown of Madness", "Darkness", "Darkvision", "Detect Thoughts", "Enhance Ability", "Enlarge/Reduce", "Enthrall", "Find Steed", "Find Traps", "Flame Blade", "Flaming Sphere", "Gentle Repose", "Gust of Wind", "Heat Metal", "Hold Person", "Invisibility", "Knock", "Lesser Restoration", "Levitate", "Locate Animals or Plants", "Locate Object", "Magic Mouth", "Magic Weapon", "Melf's Acid Arrow", "Mirror Image", "Misty Step", "Moonbeam", "Nystul's Magic Aura", "Pass without Trace", "Phantasmal Force", "Prayer of Healing", "Protection from Poison", "Ray of Enfeeblement", "Rope Trick", "Scorching Ray", "See Invisibility", "Shatter", "Silence", "Spider Climb", "Spike Growth", "Spiritual Weapon", "Suggestion", "Warding Bond", "Web", "Zone of Truth" ]
        ee_list = [ "Aganazzar's Scorcher", "Dust Devil", "Earthbind", "Maximilian's Earthen Grasp", "Pyrotechnics", "Skywrite", "Snilloc's Snowball Swarm", "Warding Wind" ]
        scag_list = []
        xge_list = [ "Aganazzar's Scorcher", "Dragon's Breath", "Dust Devil", "Earthbind", "Healing Spirit", "Maximilian's Earthen Grasp", "Mind Spike", "Pyrotechnics", "Shadow Blade", "Skywrite", "Snilloc's Snowball Swarm", "Warding Wind" ]
    elif lvl == 3:
        phb_list = [ "Animate Dead", "Aura of Vitality", "Beacon of Hope", "Bestow Curse", "Blinding Smite", "Blink", "Call Lightning", "Clairvoyance", "Conjure Animals", "Conjure Barrage", "Counterspell", "Create Food and Water", "Crusader's Mantle", "Daylight", "Dispel Magic", "Elemental Weapon", "Fear", "Feign Death", "Fireball", "Fly", "Gaseous Form", "Glyph of Warding", "Haste", "Hunger of Hadar", "Hypnotic Pattern", "Leomund's Tiny Hut", "Lightning Arrow", "Lightning Bolt", "Magic Circle", "Major Image", "Mass Healing Word", "Meld into Stone", "Nondetection", "Phantom Steed", "Plant Growth", "Protection from Energy", "Remove Curse", "Revivify", "Sending", "Sleet Storm", "Slow", "Speak with Dead", "Speak with Plants", "Spirit Guardians", "Stinking Cloud", "Tongues", "Vampiric Touch", "Water Breathing", "Water Walk", "Wind Wall" ]
        ee_list = [ "Erupting Earth", "Flame Arrows", "Melf's Minute Meteors", "Tidal Wave", "Wall of Sand", "Wall of Water" ]
        scag_list = []
        xge_list = [ "Catnap", "Enemies Abound", "Erupting Earth", "Flame Arrows", "Life Transference", "Melf's Minute Meteors", "Summon Lesser Demons", "Thunder Step", "Tidal Wave", "Tiny Servant", "Wall of Sand", "Wall of Water" ]
    elif lvl == 4:
        phb_list = [ "Arcane Eye", "Aura of Life", "Aura of Purity", "Banishment", "Blight", "Compulsion", "Confusion", "Conjure Minor Elementals", "Conjure Woodland Beings", "Control Water", "Death Ward", "Dimension Door", "Divination", "Dominate Beast", "Evard's Black Tentacles", "Fabricate", "Fire Shield", "Freedom of Movement", "Giant Insect", "Grasping Vine", "Greater Invisibility", "Guardian of Faith", "Hallucinatory Terrain", "Ice Storm", "Leomund's Secret Chest", "Locate Creature", "Mordenkainen's Faithful Hound", "Mordenkainen's Private Sanctum", "Otiluke's Resilient Sphere", "Phantasmal Killer", "Polymorph", "Staggering Smite", "Stone Shape", "Stoneskin", "Wall of Fire" ]
        ee_list = [ "Elemental Bane", "Storm Sphere", "Vitriolic Sphere", "Watery Sphere" ]
        scag_list = []
        xge_list = [ "Charm Monster", "Elemental Bane", "Find Greater Steed", "Guardian of Nature", "Shadow of Moil", "Sickening Radiance", "Storm Sphere", "Summon Greater Demon", "Vitriolic Sphere", "Watery Sphere" ]
    elif lvl == 5:
        phb_list = [ "Animate Objects", "Antilife Shell", "Awaken", "Banishing Smite", "Bigby's Hand", "Circle of Power", "Cloudkill", "Commune", "Commune with Nature", "Cone of Cold", "Conjure Elemental", "Conjure Volley", "Contact Other Plane", "Contagion", "Creation", "Destructive Wave", "Dispel Evil and Good", "Dominate Person", "Dream", "Flame Strike", "Geas", "Greater Restoration", "Hallow", "Hold Monster", "Insect Plague", "Legend Lore", "Mass Cure Wounds", "Mislead", "Modify Memory", "Passwall", "Planar Binding", "Raise Dead", "Rary's Telepathic Bond", "Reincarnate", "Scrying", "Seeming", "Swift Quiver", "Telekinesis", "Teleportation Circle", "Tree Stride", "Wall of Force", "Wall of Stone" ]
        ee_list = [ "Control Winds", "Immolation", "Maelstrom", "Transmute Rock" ]
        scag_list = []
        xge_list = [ "Danse Macabre", "Dawn", "Enervation", "Far Step", "Holy Weapon", "Immolation", "Infernal Calling", "Maelstrom", "Negative Energy Flood", "Skill Empowerment", "Steel Wind Strike", "Synaptic Static", "Transmute Rock", "Wall of Light", "Wrath of Nature" ]
    elif lvl == 6:
        phb_list = [ "Arcane Gate", "Blade Barrier", "Chain Lightning", "Circle of Death", "Conjure Fey", "Contingency", "Create Undead", "Disintegrate", "Drawmij's Instant Summons", "Eyebite", "Find the Path", "Flesh to Stone", "Forbiddance", "Globe of Invulnerability", "Guards and Wards", "Harm", "Heal", "Heroes' Feast", "Magic Jar", "Mass Suggestion", "Move Earth", "Otiluke's Freezing Sphere", "Otto's Irresistible Dance", "Planar Ally", "Programmed Illusion", "Sunbeam", "Transport via Plants", "True Seeing", "Wall of Ice", "Wall of Thorns", "Wind Walk", "Word of Recall" ]
        ee_list = [ "Bones of the Earth", "Investiture of Flame", "Investiture of Ice", "Investiture of Stone", "Investiture of Wind", "Primordial Ward" ]
        scag_list = []
        xge_list = [ "Bones of the Earth", "Create Homunculus", "Druid Grove", "Investiture of Flame", "Investiture of Ice", "Investiture of Stone", "Investiture of Wind", "Mental Prison", "Primordial Ward", "Scatter", "Soul Cage", "Tenser's Transformation" ]
    elif lvl == 7:
        phb_list = [ "Conjure Celestial", "Delayed Blast Fireball", "Divine Word", "Etherealness", "Finger of Death", "Fire Storm", "Forcecage", "Mirage Arcane", "Mordenkainen's Magnificent Mansion", "Mordenkainen's Sword", "Plane Shift", "Prismatic Spray", "Project Image", "Regenerate", "Resurrection", "Reverse Gravity", "Sequester", "Simulacrum", "Symbol", "Teleport" ]
        ee_list = [ "Whirlwind" ]
        scag_list = []
        xge_list = [ "Crown of Stars", "Power Word Pain", "Temple of the Gods", "Whirlwind" ]
    elif lvl == 8:
        phb_list = [ "Animal Shapes", "Antimagic Field", "Antipathy/Sympathy", "Clone", "Control Weather", "Demiplane", "Dominate Monster", "Earthquake", "Feeblemind", "Glibness", "Holy Aura", "Incendiary Cloud", "Maze", "Mind Blank", "Power Word Stun", "Sunburst", "Telepathy", "Tsunami" ]
        ee_list = [ "Abi-Dalzim's Horrid Wilting"]
        scag_list = []
        xge_list = [ "Abi-Dalzim's Horrid Wilting", "Illusory Dragon", "Maddening Darkness", "Mighty Fortress" ]
    elif lvl == 9:
        phb_list = [ "Astral Projection", "Foresight", "Gate", "Imprisonment", "Mass Heal", "Meteor Swarm", "Power Word Heal", "Power Word Kill", "Prismatic Wall", "Shapechange", "Storm of Vengeance", "Time Stop", "True Polymorph", "True Resurrection", "Weird", "Wish" ]
        ee_list = []
        scag_list = []
        xge_list = [ "Invulnerability", "Mass Polymorph", "Psychic Scream" ]

    scroll_list = []
    scroll_list = scroll_list + phb_list
    if "ee" in options.books:
        scroll_list = scroll_list + ee_list
    if "scag" in options.books:
        scroll_list = scroll_list + scag_list
    if "xge" in options.books:
        scroll_list = scroll_list + xge_list

    final_list = []
    for x in scroll_list:
        if x not in final_list:
            final_list.append(x)

    final_list.sort()

    length = len(final_list) - 1
    rand = random.randint(0,length)
    return(final_list[rand])

def random_armor(prof):
    phb_armor_dict = {
        'Padded': { 'prof': 'light' },
        'Leather': { 'prof': 'light' },
        'Studded leather': { 'prof': 'light' },
        'Hide': { 'prof': 'medium' },
        'Chain shirt': { 'prof': 'medium' },
        'Scale mail': { 'prof': 'medium' },
        'Breastplate': { 'prof': 'medium' },
        'Half plate': { 'prof': 'medium' },
        'Ring mail': { 'prof': 'heavy' },
        'Chain mail': { 'prof': 'heavy' },
        'Splint': { 'prof': 'heavy' },
        'Plate': { 'prof': 'heavy' },
        'Shield': { 'prof': 'shield' }
    }

    scag_armor_dict = {
        'Spiked': { 'prof': 'heavy' }
    }

    random_armor_dict = {}
    if phb:
        z = random_armor_dict.copy()
        z.update(phb_armor_dict)
        random_armor_dict = z
    if scag:
        z = random_armor_dict.copy()
        z.update(scag_armor_dict)
        random_armor_dict = z

    for x in random_armor_dict.keys():
        Prof = (str(random_armor_dict[x]['prof']) != "" and str(random_armor_dict[x]['prof']) in str(prof) or str(prof) == 'any')
        #print(x + "\n\tProf=" + str(Prof) + " prof=" + str(random_armor_dict[x]['prof']))

        if not Prof:
            del random_armor_dict[x]

    random_armor_list = []
    for x in random_armor_dict.keys():
        if x not in random_armor_list:#if not already in list
            random_armor_list.append(x)#add it to the list
    random_armor_list.sort()
    #print(random_armor_list)

    length = len(random_armor_list) - 1
    rand = random.randint(0,length)
    return(random_armor_list[rand])
    
def random_weapon(prof, dist, cat, dmg_type):
    phb_weapon_dict = {
        #simple melee weapons
        'club': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'dagger': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'greatclub': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'handaxe': { 'prof': 'simple', 'dist': 'melee', 'cat': 'axe', 'dmg_type': 'slashing' },
        'javelin': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'light hammer': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'mace': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'quarterstaff': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'sickle': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'slashing' },
        'spear': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'unarmed strike': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        #simple ranged weapons
        'light crossbow': { 'prof': 'simple', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'dart': { 'prof': 'simple', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'shortbow': { 'prof': 'simple', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'sling': { 'prof': 'simple', 'dist': 'ranged', 'cat': '', 'dmg_type': 'bludgeoning' },
        #martial melee weapons
        'battleaxe': { 'prof': 'martial', 'dist': 'melee', 'cat': 'axe', 'dmg_type': 'slashing' },
        'flail': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'glaive': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'slashing' },
        'greataxe': { 'prof': 'martial', 'dist': 'melee', 'cat': 'axe', 'dmg_type': 'slashing' },
        'greatsword': { 'prof': 'martial', 'dist': 'melee', 'cat': 'sword', 'dmg_type': 'slashing' },
        'halberd': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'slashing' },
        'lance': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'longsword': { 'prof': 'martial', 'dist': 'melee', 'cat': 'sword', 'dmg_type': 'slashing' },
        'maul': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'morningstar': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'pike': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'rapier': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'scimitar': { 'prof': 'martial', 'dist': 'melee', 'cat': 'sword', 'dmg_type': 'slashing' },
        'shortsword': { 'prof': 'martial', 'dist': 'melee', 'cat': 'sword', 'dmg_type': 'piercing' },
        'trident': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'war pick': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' },
        'warhammer': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'bludgeoning' },
        'whip': { 'prof': 'martial', 'dist': 'melee', 'cat': '', 'dmg_type': 'slashing' },
        #martial ranged weapons
        'blowgun': { 'prof': 'martial', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'hand crossbow': { 'prof': 'martial', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'heavy crossbow': { 'prof': 'martial', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'longbow': { 'prof': 'martial', 'dist': 'ranged', 'cat': '', 'dmg_type': 'piercing' },
        'net': { 'prof': 'martial', 'dist': 'ranged', 'cat': '', 'dmg_type': '' }
    }

    wge_weapon_dict = {#wayfarer's guide to eberron
        #martial melee weapons
        'double-bladed scimitar': { 'prof': 'martial', 'dist': 'melee', 'cat': 'sword', 'dmg_type': 'slashing' }
    }

    toa_weapon_dict = {#tomb of annihilation
        #simple melee weapons
        'yklwa': { 'prof': 'simple', 'dist': 'melee', 'cat': '', 'dmg_type': 'piercing' }
    }

    ren_firearms_dict = {
        'pistol': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' },
        'musket': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' }
    }

    modern_firearms_dict = {
        'pistol, automatic': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' },
        'revolver': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' },
        'rifle, hunting': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' },
        'rifle, automatic': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' },
        'shotgun': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'piercing' }
    }

    futuristic_firearms_dict = {
        'laser pistol': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'radiant' },
        'antimatter rifle': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'necrotic' },
        'laser rifle': { 'prof': 'martial', 'dist': 'ranged', 'cat': 'firearm', 'dmg_type': 'radiant' }
    }

    random_weapon_dict = {}

    if phb:
        z = random_weapon_dict.copy()
        z.update(phb_weapon_dict)
        random_weapon_dict = z
    if ren_firearms:
        z = random_weapon_dict.copy()
        z.update(ren_firearms_dict)
        random_weapon_dict = z
    if modern_firearms:
        z = random_weapon_dict.copy()
        z.update(modern_firearms_dict)
        random_weapon_dict = z
    if futuristic_firearms:
        z = random_weapon_dict.copy()
        z.update(futuristic_firearms_dict)
        random_weapon_dict = z
    if wge:
        z = random_weapon_dict.copy()
        z.update(wge_weapon_dict)
        random_weapon_dict = z
    if toa:
        z = random_weapon_dict.copy()
        z.update(toa_weapon_dict)
        random_weapon_dict = z

    for x in random_weapon_dict.keys():
        Prof = (str(random_weapon_dict[x]['prof']) != "" and str(prof) == str(random_weapon_dict[x]['prof']) or str(prof) == 'any')
        Dist = (str(random_weapon_dict[x]['dist']) != "" and str(dist) == str(random_weapon_dict[x]['dist']) or str(dist) == 'any')
        Cat = (str(random_weapon_dict[x]['cat']) != "" and str(random_weapon_dict[x]['cat']) in str(cat) or str(cat) == 'any')
        Dmg_Type = (str(random_weapon_dict[x]['dmg_type']) != "" and str(dmg_type) == str(random_weapon_dict[x]['dmg_type']) or str(dmg_type) == 'any')
        #print(x + "\n\tProf=" + str(Prof) + " prof=" + str(random_weapon_dict[x]['prof']) + "\n\tDist=" + str(Dist) + " dist=" + str(random_weapon_dict[x]['dist']) + "\n\tCat=" + str(Cat) + " cat=" + str(random_weapon_dict[x]['cat']) + "\n\tDmg_Type=" + str(Dmg_Type) + " dmg_type=" + str(random_weapon_dict[x]['dmg_type']))

        if not Prof or not Dist or not Cat or not Dmg_Type:
            del random_weapon_dict[x]

    random_weapon_list = []
    for x in random_weapon_dict.keys():
        if x not in random_weapon_list:#if not already in list
            random_weapon_list.append(x)#add it to the list
    random_weapon_list.sort()
    #print(random_weapon_list)

    length = len(random_weapon_list) - 1
    rand = random.randint(0,length)
    return(random_weapon_list[rand])

def random_ammunition(prof, cat, dmg_type):
    phb_ammo_dict = {
        'light crossbow bolts': { 'prof': 'simple', 'cat': '', 'dmg_type': 'piercing' },
        'shortbow arrows': { 'prof': 'simple', 'cat': '', 'dmg_type': 'piercing' },
        'sling bullets': { 'prof': 'simple', 'cat': '', 'dmg_type': 'bludgeoning' },
        'blowgun darts': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'hand crossbow bolts': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'heavy crossbow bolts': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'longbow arrows': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' }
    }

    ren_firearms_ammo_dict = {
        'pistol bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'musket bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' }
    }

    modern_firearms_ammo_dict = {
        'automatic pistol bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'revolver bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'hunting rifle bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'automatic rifle bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
        'shotgun bullets': { 'prof': 'martial', 'cat': '', 'dmg_type': 'piercing' },
    }

    futuristic_firearms_ammo_dict = {
        'laser pistol energy cell': { 'prof': 'martial', 'cat': '', 'dmg_type': 'radiant' },
        'antimatter rifle energy cell': { 'prof': 'martial', 'cat': '', 'dmg_type': 'necrotic' },
        'laser rifle energy cell': { 'prof': 'martial', 'cat': '', 'dmg_type': 'radiant' },
    }

    random_ammo_dict = {}

    if phb:
        z = random_ammo_dict.copy()
        z.update(phb_ammo_dict)
        random_ammo_dict = z
    if ren_firearms:
        z = random_ammo_dict.copy()
        z.update(ren_firearms_ammo_dict)
        random_ammo_dict = z
    if modern_firearms:
        z = random_ammo_dict.copy()
        z.update(modern_firearms_ammo_dict)
        random_ammo_dict = z
    if futuristic_firearms:
        z = random_ammo_dict.copy()
        z.update(futuristic_firearms_ammo_dict)
        random_ammo_dict = z

    for x in random_ammo_dict.keys():
        Prof = (str(random_ammo_dict[x]['prof']) != "" and str(prof) == str(random_ammo_dict[x]['prof']) or str(prof) == 'any')
        Cat = (str(random_ammo_dict[x]['cat']) != "" and str(random_ammo_dict[x]['cat']) in str(cat) or str(cat) == 'any')
        Dmg_Type = (str(random_ammo_dict[x]['dmg_type']) != "" and str(dmg_type) == str(random_ammo_dict[x]['dmg_type']) or str(dmg_type) == 'any')
        #print(x + "\n\tProf=" + str(Prof) + " prof=" + str(random_ammo_dict[x]['prof']) + "\n\tCat=" + str(Cat) + " cat=" + str(random_ammo_dict[x]['cat']) + "\n\tDmg_Type=" + str(Dmg_Type) + " dmg_type=" + str(random_ammo_dict[x]['dmg_type']))

        if not Prof or not Cat or not Dmg_Type:
            del random_ammo_dict[x]

    random_ammo_list = []
    for x in random_ammo_dict.keys():
        if x not in random_ammo_list:#if not already in list
            random_ammo_list.append(x)#add it to the list
    random_ammo_list.sort()
    #print(random_ammo_list)

    length = len(random_ammo_list) - 1
    rand = random.randint(0,length)
    return(random_ammo_list[rand])

def magic_item_table_a(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 50:
            treasure_final_result.append("Potion of healing")
        elif roll >= 51 and roll <= 60:
            treasure_final_result.append("cantrip Spell scroll (" + random_spell(0) + ")")
        elif roll >= 61 and roll <= 70:
            treasure_final_result.append("Potion of climbing")
        elif roll >= 71 and roll <= 90:
            treasure_final_result.append("1st-level Spell scroll (" + random_spell(1) + ")")
        elif roll >= 91 and roll <= 94:
            treasure_final_result.append("2nd-level Spell scroll (" + random_spell(2) + ")")
        elif roll >= 95 and roll <= 98:
            treasure_final_result.append("Potion of greater healing")
        elif roll == 99:
            treasure_final_result.append("Bag of holding")
        elif roll == 100:
            treasure_final_result.append("Driftglobe")

def magic_item_table_b(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 15:
            treasure_final_result.append("Potion of greater healing")
        elif roll >= 16 and roll <= 22:
            treasure_final_result.append("Potion of fire breath")
        elif roll >= 23 and roll <= 29:
            treasure_final_result.append("Potion of resistance")
        elif roll >= 30 and roll <= 34:
            treasure_final_result.append("Ammunition, +1 (" + random_ammunition('any','any','any') + ")")
        elif roll >= 35 and roll <= 39:
            treasure_final_result.append("Potion of animal friendship")
        elif roll >= 40 and roll <= 44:
            treasure_final_result.append("Potion of hill giant strength")
        elif roll >= 45 and roll <= 49:
            treasure_final_result.append("Potion of growth")
        elif roll >= 50 and roll <= 54:
            treasure_final_result.append("Potion of water breathing")
        elif roll >= 55 and roll <= 59:
            treasure_final_result.append("2nd-level Spell scroll (" + random_spell(2) + ")")
        elif roll >= 60 and roll <= 64:
            treasure_final_result.append("3rd-level Spell scroll (" + random_spell(3) + ")")
        elif roll >= 65 and roll <= 69:
            treasure_final_result.append("Bag of holding")
        elif roll >= 68 and roll <= 70:
            treasure_final_result.append("Keoghtom's ointment")
        elif roll >= 71 and roll <= 73:
            treasure_final_result.append("Oil of slipperiness")
        elif roll >= 74 and roll <= 75:
            treasure_final_result.append("Dust of disappearance")
        elif roll >= 76 and roll <= 77:
            treasure_final_result.append("Dust of dryness")
        elif roll >= 78 and roll <= 79:
            treasure_final_result.append("Dust of sneezing and choking")
        elif roll >= 80 and roll <= 81:
            treasure_final_result.append("Elemental gem")
        elif roll >= 82 and roll <= 83:
            treasure_final_result.append("Philter of love")
        elif roll == 84:
            treasure_final_result.append("Alchemy jug")
        elif roll == 85:
            treasure_final_result.append("Cap of water breathing")
        elif roll == 86:
            treasure_final_result.append("Cloak of the manta ray")
        elif roll == 87:
            treasure_final_result.append("Driftglobe")
        elif roll == 88:
            treasure_final_result.append("Goggles of night")
        elif roll == 89:
            treasure_final_result.append("Helm of comprehending languages")
        elif roll == 90:
            treasure_final_result.append("Immovable rod")
        elif roll == 91:
            treasure_final_result.append("Lantern of revealing")
        elif roll == 92:
            treasure_final_result.append("Mariner's armor (" + random_armor('any') + ")")
        elif roll == 93:
            x = random_armor('medium or heavy')
            while x == 'Hide':#not hide
                x = random_armor('medium or heavy')
            treasure_final_result.append("Mithral armor (" + x + ")")
        elif roll == 94:
            treasure_final_result.append("Potion of poison")
        elif roll == 95:
            treasure_final_result.append("Ring of swimming")
        elif roll == 96:
            treasure_final_result.append("Robe of useful items")
        elif roll == 97:
            treasure_final_result.append("Rope of climbing")
        elif roll == 98:
            treasure_final_result.append("Saddle of the cavalier")
        elif roll == 99:
            treasure_final_result.append("Wand of magic detection")
        elif roll == 100:
            treasure_final_result.append("Wand of secrets")

def magic_item_table_c(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 15:
            treasure_final_result.append("Potion of superior healing")
        elif roll >= 16 and roll <= 22:
            treasure_final_result.append("4th-level Spell scroll (" + random_spell(4) + ")")
        elif roll >= 23 and roll <= 27:
            treasure_final_result.append("Ammunition, +2 (" + random_ammunition('any','any','any') + ")")
        elif roll >= 28 and roll <= 32:
            treasure_final_result.append("Potion of clairvoyance")
        elif roll >= 33 and roll <= 37:
            treasure_final_result.append("Potion of diminution")
        elif roll >= 38 and roll <= 42:
            treasure_final_result.append("Potion of gaseous form")
        elif roll >= 43 and roll <= 47:
            treasure_final_result.append("Potion of frost giant strength")
        elif roll >= 48 and roll <= 52:
            treasure_final_result.append("Potion of stone giant strength")
        elif roll >= 53 and roll <= 57:
            treasure_final_result.append("Potion of heroism")
        elif roll >= 58 and roll <= 62:
            treasure_final_result.append("Potion of invulnerability")
        elif roll >= 63 and roll <= 67:
            treasure_final_result.append("Potion of mind reading")
        elif roll >= 68 and roll <= 72:
            treasure_final_result.append("5th-level Spell scroll (" + random_spell(5) + ")")
        elif roll >= 73 and roll <= 75:
            treasure_final_result.append("Elixir of health")
        elif roll >= 76 and roll <= 78:
            treasure_final_result.append("Oil of etherealness")
        elif roll >= 79 and roll <= 81:
            treasure_final_result.append("Potion of fire giant strength")
        elif roll >= 82 and roll <= 84:
            treasure_final_result.append("Quaal's feather token")
        elif roll >= 85 and roll <= 87:
            treasure_final_result.append("Scroll of protection")
        elif roll >= 88 and roll <= 89:
            treasure_final_result.append("Bag of beans")
        elif roll >= 90 and roll <= 91:
            treasure_final_result.append("Bead of force")
        elif roll == 92:
            treasure_final_result.append("Chime of opening")
        elif roll == 93:
            treasure_final_result.append("Decanter of endless water")
        elif roll == 94:
            treasure_final_result.append("Eyes of minute seeing")
        elif roll == 95:
            treasure_final_result.append("Folding boat")
        elif roll == 96:
            treasure_final_result.append("Heward's handy haversack")
        elif roll == 97:
            treasure_final_result.append("Horseshoes of speed")
        elif roll == 98:
            treasure_final_result.append("Necklace of fireballs")
        elif roll == 99:
            treasure_final_result.append("Periapt of health")
        elif roll == 100:
            treasure_final_result.append("Sending stones")

def magic_item_table_d(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 20:
            treasure_final_result.append("Potion of supreme healing")
        elif roll >= 21 and roll <= 30:
            treasure_final_result.append("Potion of invisibility")
        elif roll >= 31 and roll <= 40:
            treasure_final_result.append("Potion of speed")
        elif roll >= 41 and roll <= 50:
            treasure_final_result.append("6th-level Spell scroll (" + random_spell(6) + ")")
        elif roll >= 51 and roll <= 57:
            treasure_final_result.append("7th-level Spell scroll (" + random_spell(7) + ")")
        elif roll >= 58 and roll <= 62:
            treasure_final_result.append("Ammunition, +3 (" + random_ammunition('any','any','any') + ")")
        elif roll >= 63 and roll <= 67:
            treasure_final_result.append("Oil of sharpness")
        elif roll >= 68 and roll <= 72:
            treasure_final_result.append("Potion of flying")
        elif roll >= 73 and roll <= 77:
            treasure_final_result.append("Potion of cloud giant strength")
        elif roll >= 78 and roll <= 82:
            treasure_final_result.append("Potion of longevity")
        elif roll >= 83 and roll <= 87:
            treasure_final_result.append("Potion of vitality")
        elif roll >= 88 and roll <= 92:
            treasure_final_result.append("8th-level Spell scroll (" + random_spell(8) + ")")
        elif roll >= 93 and roll <= 95:
            treasure_final_result.append("Horseshoes of a zephyr")
        elif roll >= 96 and roll <= 98:
            treasure_final_result.append("Nolzur's marvelous pigments")
        elif roll == 99:
            treasure_final_result.append("Bag of devouring")
        elif roll == 100:
            treasure_final_result.append("Portable hole")

def magic_item_table_e(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 30:
            treasure_final_result.append("8th-level Spell scroll (" + random_spell(8) + ")")
        elif roll >= 31 and roll <= 55:
            treasure_final_result.append("Potion of storm giant strength")
        elif roll >= 56 and roll <= 70:
            treasure_final_result.append("Potion of supreme healing")
        elif roll >= 71 and roll <= 85:
            treasure_final_result.append("9th-level Spell scroll (" + random_spell(9) + ")")
        elif roll >= 86 and roll <= 93:
            treasure_final_result.append("Universal solvent")
        elif roll >= 94 and roll <= 98:
            x = random_ammunition('any','any','any')
            x = x.rstrip('s')
            treasure_final_result.append(x + " of slaying")
        elif roll >= 99 and roll <= 100:
            treasure_final_result.append("Sovereign glue")

def magic_item_table_f(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 15:
            treasure_final_result.append("Weapon, +1 (" + random_weapon('any','any','any','any') + ")")
        elif roll >= 16 and roll <= 18:
            treasure_final_result.append("Shield, +1")
        elif roll >= 19 and roll <= 21:
            treasure_final_result.append("Sentinel shield")
        elif roll >= 22 and roll <= 23:
            treasure_final_result.append("Amulet of proof against detection and location")
        elif roll >= 24 and roll <= 25:
            treasure_final_result.append("Boots of elvenkind")
        elif roll >= 26 and roll <= 27:
            treasure_final_result.append("Boots of striding and springing")
        elif roll >= 28 and roll <= 29:
            treasure_final_result.append("Bracers of archery")
        elif roll >= 30 and roll <= 31:
            treasure_final_result.append("Brooch of shielding")
        elif roll >= 32 and roll <= 33:
            treasure_final_result.append("Broom of flying")
        elif roll >= 34 and roll <= 35:
            treasure_final_result.append("Cloak of elvenkind")
        elif roll >= 36 and roll <= 37:
            treasure_final_result.append("Cloak of protection")
        elif roll >= 38 and roll <= 39:
            treasure_final_result.append("Gauntlets of ogre power")
        elif roll >= 40 and roll <= 41:
            treasure_final_result.append("Hat of disguise")
        elif roll >= 42 and roll <= 43:
            treasure_final_result.append("Javelin of lightning")
        elif roll >= 44 and roll <= 45:
            treasure_final_result.append("Pearl of power")
        elif roll >= 46 and roll <= 47:
            treasure_final_result.append("Rod of the pact keeper, + 1")
        elif roll >= 48 and roll <= 49:
            treasure_final_result.append("Slippers of spider climbing")
        elif roll >= 50 and roll <= 51:
            treasure_final_result.append("Staff of the adder")
        elif roll >= 52 and roll <= 53:
            treasure_final_result.append("Staff of the python")
        elif roll >= 54 and roll <= 55:
            treasure_final_result.append("Sword of vengeance (" + random_weapon('any','any','sword','any') + ")")
        elif roll >= 56 and roll <= 57:
            treasure_final_result.append("Trident of fish command")
        elif roll >= 58 and roll <= 59:
            treasure_final_result.append("Wand of magic missiles")
        elif roll >= 60 and roll <= 61:
            treasure_final_result.append("Wand of the war mage, + 1")
        elif roll >= 62 and roll <= 63:
            treasure_final_result.append("Wand of web")
        elif roll >= 64 and roll <= 65:
            treasure_final_result.append("Weapon of warning (" + random_weapon('any','any','any','any') + ")")
        elif roll == 66:
            treasure_final_result.append("Adamantine armor (chain mail)")
        elif roll == 67:
            treasure_final_result.append("Adamantine armor (chain shirt)")
        elif roll == 68:
            treasure_final_result.append("Adamantine armor (scale mail)")
        elif roll == 69:
            treasure_final_result.append("Bag of tricks (gray)")
        elif roll == 70:
            treasure_final_result.append("Bag of tricks (rust)")
        elif roll == 71:
            treasure_final_result.append("Bag of tricks (tan)")
        elif roll == 72:
            treasure_final_result.append("Boots of the winterlands")
        elif roll == 73:
            treasure_final_result.append("Circlet of blasting")
        elif roll == 74:
            treasure_final_result.append("Deck of illusions")
        elif roll == 75:
            treasure_final_result.append("Eversmoking bottle")
        elif roll == 76:
            treasure_final_result.append("Eyes of charming")
        elif roll == 77:
            treasure_final_result.append("Eyes of the eagle")
        elif roll == 78:
            treasure_final_result.append("Figurine of wondrous power (silver raven)")
        elif roll == 79:
            treasure_final_result.append("Gem of brightness")
        elif roll == 80:
            treasure_final_result.append("Gloves of missile snaring")
        elif roll == 81:
            treasure_final_result.append("Gloves of swimming and climbing")
        elif roll == 82:
            treasure_final_result.append("Gloves of thievery")
        elif roll == 83:
            treasure_final_result.append("Headband of intellect")
        elif roll == 84:
            treasure_final_result.append("Helm of telepathy")
        elif roll == 85:
            treasure_final_result.append("Instrument of the bards (Doss lute)")
        elif roll == 86:
            treasure_final_result.append("Instrument of the bards (Fochlucan bandore)")
        elif roll == 87:
            treasure_final_result.append("Instrument of the bards (Mac-Fuimidh cittern)")
        elif roll == 88:
            treasure_final_result.append("Medallion of thoughts")
        elif roll == 89:
            treasure_final_result.append("Necklace of adaptation")
        elif roll == 90:
            treasure_final_result.append("Periapt of wound closure")
        elif roll == 91:
            treasure_final_result.append("Pipes of haunting")
        elif roll == 92:
            treasure_final_result.append("Pipes of the sewers")
        elif roll == 93:
            treasure_final_result.append("Ring of jumping")
        elif roll == 94:
            treasure_final_result.append("Ring of mind shielding")
        elif roll == 95:
            treasure_final_result.append("Ring of warmth")
        elif roll == 96:
            treasure_final_result.append("Ring of water walking")
        elif roll == 97:
            treasure_final_result.append("Quiver of Ehlonna")
        elif roll == 98:
            treasure_final_result.append("Stone of good luck")
        elif roll == 99:
            treasure_final_result.append("Wind fan")
        elif roll == 100:
            treasure_final_result.append("Winged boot")

def magic_item_table_g(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 11:
            treasure_final_result.append("Weapon, +2 (" + random_weapon('any','any','any','any') + ")")
        elif roll >= 12 and roll <= 14:
            desc = "Figurine of wondrous power "
            desc_roll = random.randint(1,8)
            if desc_roll == 1:
                treasure_final_result.append(desc + "(Bronze griffon)")
            elif desc_roll == 2:
                treasure_final_result.append(desc + "(Ebony fly)")
            elif desc_roll == 3:
                treasure_final_result.append(desc + "(Golden lions)")
            elif desc_roll == 4:
                treasure_final_result.append(desc + "(Ivory goats)")
            elif desc_roll == 5:
                treasure_final_result.append(desc + "(Marble elephant)")
            elif desc_roll >= 6 and desc_roll <= 7:
                treasure_final_result.append(desc + "(Onyx dog)")
            elif desc_roll == 8:
                treasure_final_result.append(desc + "(Serpentine owl)")
        elif roll == 15:
            treasure_final_result.append("Adamantine armor (breastplate)")
        elif roll == 16:
            treasure_final_result.append("Adamantine armor (splint)")
        elif roll == 17:
            treasure_final_result.append("Amulet of health")
        elif roll == 18:
            treasure_final_result.append("Armor of vulnerability")
        elif roll == 19:
            treasure_final_result.append("Arrow-catching shield")
        elif roll == 20:
            treasure_final_result.append("Belt of dwarvenkind")
        elif roll == 21:
            treasure_final_result.append("Belt of hill giant strength")
        elif roll == 22:
            treasure_final_result.append("Berserker axe (" + random_weapon('any','any','axe','any') + ")")
        elif roll == 23:
            treasure_final_result.append("Boots of levitation")
        elif roll == 24:
            treasure_final_result.append("Boots of speed")
        elif roll == 25:
            treasure_final_result.append("Bowl of commanding water elementals")
        elif roll == 26:
            treasure_final_result.append("Bracers of defense")
        elif roll == 27:
            treasure_final_result.append("Brazier of commanding fire elementals")
        elif roll == 28:
            treasure_final_result.append("Cape of the mountebank")
        elif roll == 29:
            treasure_final_result.append("Censer of controlling air elementals")
        elif roll == 30:
            treasure_final_result.append("Armor, +1 chain mail")
        elif roll == 31:
            treasure_final_result.append("Armor of resistance (chain mail)")
        elif roll == 32:
            treasure_final_result.append("Armor, +1 chain shirt")
        elif roll == 33:
            treasure_final_result.append("Armor of resistance (chain shirt)")
        elif roll == 34:
            treasure_final_result.append("Cloak of displacement")
        elif roll == 35:
            treasure_final_result.append("Cloak of the bat")
        elif roll == 36:
            treasure_final_result.append("Cube afforce")
        elif roll == 37:
            treasure_final_result.append("Daern's instant fortress")
        elif roll == 38:
            treasure_final_result.append("Dagger of venom")
        elif roll == 39:
            treasure_final_result.append("Dimensional shackles")
        elif roll == 40:
            treasure_final_result.append("Dragon slayer (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 41:
            treasure_final_result.append("Elven chain")
        elif roll == 42:
            treasure_final_result.append("Flame tongue (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 43:
            treasure_final_result.append("Gem of seeing")
        elif roll == 44:
            treasure_final_result.append("Giant slayer (" + random_weapon('any','any','axe or sword','any') + ")")
        elif roll == 45:
            treasure_final_result.append("Clamoured studded leather")
        elif roll == 46:
            treasure_final_result.append("Helm of teleportation")
        elif roll == 47:
            treasure_final_result.append("Horn of blasting")
        elif roll == 48:
            desc_roll = random.randint(1,2)
            if desc_roll == 1:
                desc = "silver"
            elif desc_roll == 2:
                desc = "brass"
            treasure_final_result.append("Horn of Valhalla (" + desc + ")")
        elif roll == 49:
            treasure_final_result.append("Instrument of the bards (Canaith mandolin)")
        elif roll == 50:
            treasure_final_result.append("Instrument ofthe bards (Cli lyre)")
        elif roll == 51:
            treasure_final_result.append("Ioun stone (awareness)")
        elif roll == 52:
            treasure_final_result.append("Ioun stone (protection)")
        elif roll == 53:
            treasure_final_result.append("Ioun stone (reserve)")
        elif roll == 54:
            treasure_final_result.append("Ioun stone (sustenance)")
        elif roll == 55:
            treasure_final_result.append("Iron bands of Bilarro")
        elif roll == 56:
            treasure_final_result.append("Armor, +1 leather")
        elif roll == 57:
            treasure_final_result.append("Armor of resistance (leather)")
        elif roll == 58:
            treasure_final_result.append("Mace of disruption")
        elif roll == 59:
            treasure_final_result.append("Mace of smiting")
        elif roll == 60:
            treasure_final_result.append("Mace of terror")
        elif roll == 61:
            treasure_final_result.append("Mantle of spell resistance")
        elif roll == 62:
            treasure_final_result.append("Necklace of prayer beads")
        elif roll == 63:
            treasure_final_result.append("Periapt of proof against poison")
        elif roll == 64:
            treasure_final_result.append("Ring of animal influence")
        elif roll == 65:
            treasure_final_result.append("Ring of evasion")
        elif roll == 66:
            treasure_final_result.append("Ring of feather falling")
        elif roll == 67:
            treasure_final_result.append("Ring of free action")
        elif roll == 68:
            treasure_final_result.append("Ring of protection")
        elif roll == 69:
            treasure_final_result.append("Ring of resistance")
        elif roll == 70:
            treasure_final_result.append("Ring of spell storing")
        elif roll == 71:
            treasure_final_result.append("Ring of the ram")
        elif roll == 72:
            treasure_final_result.append("Ring of X-ray vision")
        elif roll == 73:
            treasure_final_result.append("Robe of eyes")
        elif roll == 74:
            treasure_final_result.append("Rod of rulership")
        elif roll == 75:
            treasure_final_result.append("Rod of the pact keeper, +2")
        elif roll == 76:
            treasure_final_result.append("Rope of entanglement")
        elif roll == 77:
            treasure_final_result.append("Armor, +1 scale mail")
        elif roll == 78:
            treasure_final_result.append("Armor of resistance (scale mail)")
        elif roll == 79:
            treasure_final_result.append("Shield, +2")
        elif roll == 80:
            treasure_final_result.append("Shield of missile attraction")
        elif roll == 81:
            treasure_final_result.append("Staff of charming")
        elif roll == 82:
            treasure_final_result.append("Staff of healing")
        elif roll == 83:
            treasure_final_result.append("Staff of swarming insects")
        elif roll == 84:
            treasure_final_result.append("Staff of the woodlands")
        elif roll == 85:
            treasure_final_result.append("Staff of withering")
        elif roll == 86:
            treasure_final_result.append("Stone of controlling earth elementals")
        elif roll == 87:
            treasure_final_result.append("Sun blade")
        elif roll == 88:
            treasure_final_result.append("Sword of life stealing (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 89:
            treasure_final_result.append("Sword of wounding (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 90:
            treasure_final_result.append("Tentacle rod")
        elif roll == 91:
            treasure_final_result.append("Vicious weapon (" + random_weapon('any','any','any','any') + ")")
        elif roll == 92:
            treasure_final_result.append("Wand of binding")
        elif roll == 93:
            treasure_final_result.append("Wand of enemy detection")
        elif roll == 94:
            treasure_final_result.append("Wand of fear")
        elif roll == 95:
            treasure_final_result.append("Wand of fireballs")
        elif roll == 96:
            treasure_final_result.append("Wand of lightning bolts")
        elif roll == 97:
            treasure_final_result.append("Wand of paralysis")
        elif roll == 98:
            treasure_final_result.append("Wand of the war mage, +2")
        elif roll == 99:
            treasure_final_result.append("Wand of wonder")
        elif roll == 100:
            treasure_final_result.append("Wings of flying")

def magic_item_table_h(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 10:
            treasure_final_result.append("Weapon, +3 (" + random_weapon('any','any','any','any') + ")")
        elif roll >= 11 and roll <= 12:
            treasure_final_result.append("Amulet of the planes")
        elif roll >= 11 and roll <= 14:
            treasure_final_result.append("Carpet of flying")
        elif roll >= 11 and roll <= 16:
            treasure_final_result.append("Crystal ball (very rare version)")
        elif roll >= 11 and roll <= 18:
            treasure_final_result.append("Ring of regeneration")
        elif roll >= 11 and roll <= 20:
            treasure_final_result.append("Ring of shooting stars")
        elif roll >= 11 and roll <= 22:
            treasure_final_result.append("Ring of telekinesis")
        elif roll >= 11 and roll <= 24:
            treasure_final_result.append("Robe of scintillating colors")
        elif roll >= 11 and roll <= 26:
            treasure_final_result.append("Robe of stars")
        elif roll >= 11 and roll <= 28:
            treasure_final_result.append("Rod of absorption")
        elif roll >= 11 and roll <= 30:
            treasure_final_result.append("Rod of alertness")
        elif roll >= 11 and roll <= 32:
            treasure_final_result.append("Rod of security")
        elif roll >= 11 and roll <= 34:
            treasure_final_result.append("Rod of the pact keeper, +3")
        elif roll >= 11 and roll <= 36:
            treasure_final_result.append("Scimitar of speed")
        elif roll >= 11 and roll <= 38:
            treasure_final_result.append("Shield, +3")
        elif roll >= 11 and roll <= 40:
            treasure_final_result.append("Staff of fire")
        elif roll >= 11 and roll <= 42:
            treasure_final_result.append("Staff of frost")
        elif roll >= 11 and roll <= 44:
            treasure_final_result.append("Staff of power")
        elif roll >= 11 and roll <= 46:
            treasure_final_result.append("Staff of striking")
        elif roll >= 11 and roll <= 48:
            treasure_final_result.append("Staff of thunder and lightning")
        elif roll >= 11 and roll <= 50:
            treasure_final_result.append("Sword of sharpness (" + random_weapon('any','any','sword','slashing') + ")")
        elif roll >= 11 and roll <= 52:
            treasure_final_result.append("Wand of polymorph")
        elif roll >= 11 and roll <= 54:
            treasure_final_result.append("Wand of the war mage, +3")
        elif roll == 55:
            treasure_final_result.append("Adamantine armor (half plate)")
        elif roll == 56:
            treasure_final_result.append("Adamantine armor (plate)")
        elif roll == 57:
            treasure_final_result.append("Animated shield")
        elif roll == 58:
            treasure_final_result.append("Belt of fire giant strength")
        elif roll == 59:
            treasure_final_result.append("Belt of frost (or stone) giant strength")
        elif roll == 60:
            treasure_final_result.append("Armor, +1 breastplate")
        elif roll == 61:
            treasure_final_result.append("Armor of resistance (breastplate)")
        elif roll == 62:
            treasure_final_result.append("Candle of invocation")
        elif roll == 63:
            treasure_final_result.append("Armor, +2 chain mail")
        elif roll == 64:
            treasure_final_result.append("Armor, +2 chain shirt")
        elif roll == 65:
            treasure_final_result.append("Cloak of arachnida")
        elif roll == 66:
            treasure_final_result.append("Dancing sword (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 67:
            treasure_final_result.append("Demon armor")
        elif roll == 68:
            treasure_final_result.append("Dragon scale mail")
        elif roll == 69:
            treasure_final_result.append("Dwarven plate")
        elif roll == 70:
            treasure_final_result.append("Dwarven thrower")
        elif roll == 71:
            treasure_final_result.append("Efreeti bottle")
        elif roll == 72:
            treasure_final_result.append("Figurine of wondrous power (obsidian steed)")
        elif roll == 73:
            treasure_final_result.append("Frost brand")
        elif roll == 74:
            treasure_final_result.append("Helm of brilliance")
        elif roll == 75:
            treasure_final_result.append("Horn of Valhalla (bronze)")
        elif roll == 76:
            treasure_final_result.append("Instrument of the bards (Anstruth harp)")
        elif roll == 77:
            treasure_final_result.append("Ioun stone (absorption)")
        elif roll == 78:
            treasure_final_result.append("Ioun stone (agility)")
        elif roll == 79:
            treasure_final_result.append("Ioun stone (fortitude)")
        elif roll == 80:
            treasure_final_result.append("Ioun stone (insight)")
        elif roll == 81:
            treasure_final_result.append("Ioun stone (intellect)")
        elif roll == 82:
            treasure_final_result.append("Ioun stone (leadership)")
        elif roll == 83:
            treasure_final_result.append("Ioun stone (strength)")
        elif roll == 84:
            treasure_final_result.append("Armor, +2 leather")
        elif roll == 85:
            treasure_final_result.append("Manual of bodily health")
        elif roll == 86:
            treasure_final_result.append("Manual of gainful exercise")
        elif roll == 87:
            treasure_final_result.append("Manual of golems")
        elif roll == 88:
            treasure_final_result.append("Manual of quickness of action")
        elif roll == 89:
            treasure_final_result.append("Mirror of life trapping")
        elif roll == 90:
            treasure_final_result.append("Nine lives stealer (" + random_weapon('any','any','sword','any') + ")")
        elif roll == 91:
            treasure_final_result.append("Oath bow")
        elif roll == 92:
            treasure_final_result.append("Armor, +2 scale mail")
        elif roll == 93:
            treasure_final_result.append("Spellguard shield")
        elif roll == 94:
            treasure_final_result.append("Armor, +1 splint")
        elif roll == 95:
            treasure_final_result.append("Armor of resistance (splint)")
        elif roll == 96:
            treasure_final_result.append("Armor, +1 studded leather")
        elif roll == 97:
            treasure_final_result.append("Armor of resistance (studded leather)")
        elif roll == 98:
            treasure_final_result.append("Tome of clear thought")
        elif roll == 99:
            treasure_final_result.append("Tome of leadership and influence")
        elif roll == 100:
            treasure_final_result.append("Tome of understanding")

def magic_item_table_i(num, die):
    rng = random.randint(num,num*die)
    for x in range(rng):
        roll = random.randint(1,100) #1d100

        if roll >= 1 and roll <= 5:
            treasure_final_result.append("Defender (" + random_weapon('any','any','sword','any') + ")")
        elif roll >= 6 and roll <= 10:
            treasure_final_result.append("Hammer of thunderbolts")
        elif roll >= 11 and roll <= 15:
            treasure_final_result.append("Luck blade (" + random_weapon('any','any','sword','any') + ")")
        elif roll >= 16 and roll <= 20:
            treasure_final_result.append("Sword of answering")
        elif roll >= 21 and roll <= 23:
            treasure_final_result.append("Holy avenger (" + random_weapon('any','any','sword','any') + ")")
        elif roll >= 24 and roll <= 26:
            treasure_final_result.append("Ring of djinni summoning")
        elif roll >= 27 and roll <= 29:
            treasure_final_result.append("Ring of invisibility")
        elif roll >= 30 and roll <= 32:
            treasure_final_result.append("Ring of spell turning")
        elif roll >= 33 and roll <= 35:
            treasure_final_result.append("Rod of lordly might")
        elif roll >= 36 and roll <= 38:
            treasure_final_result.append("Staff of the magi")
        elif roll >= 39 and roll <= 41:
            treasure_final_result.append("Vorpal sword (" + random_weapon('any','any','sword', 'slashing') + ")")
        elif roll >= 42 and roll <= 43:
            treasure_final_result.append("Belt of cloud giant strength")
        elif roll >= 44 and roll <= 45:
            treasure_final_result.append("Armor, +2 breastplate")
        elif roll >= 46 and roll <= 47:
            treasure_final_result.append("Armor, +3 chain mail")
        elif roll >= 48 and roll <= 49:
            treasure_final_result.append("Armor, +3 chain shirt")
        elif roll >= 50 and roll <= 51:
            treasure_final_result.append("Cloak of invisibility")
        elif roll >= 52 and roll <= 53:
            treasure_final_result.append("Crystal ball (legendary version)")
        elif roll >= 54 and roll <= 55:
            treasure_final_result.append("Armor, +1 half plate")
        elif roll >= 56 and roll <= 57:
            treasure_final_result.append("Iron flask")
        elif roll >= 58 and roll <= 59:
            treasure_final_result.append("Armor, +3 leather")
        elif roll >= 60 and roll <= 61:
            treasure_final_result.append("Armor, +1 plate")
        elif roll >= 61 and roll <= 63:
            treasure_final_result.append("Robe of the archmagi")
        elif roll >= 64 and roll <= 65:
            treasure_final_result.append("Rod of resurrection")
        elif roll >= 66 and roll <= 67:
            treasure_final_result.append("Armor, +1 scale mail")
        elif roll >= 68 and roll <= 69:
            treasure_final_result.append("Scarab of protection")
        elif roll >= 70 and roll <= 71:
            treasure_final_result.append("Armor, +2 splint")
        elif roll >= 72 and roll <= 73:
            treasure_final_result.append("Armor, +2 studded leather")
        elif roll >= 74 and roll <= 75:
            treasure_final_result.append("Well of many worlds")
        elif roll == 76:
            desc_roll = random.randint(1,12)
            if desc_roll >= 1 and desc_roll <= 2:
                treasure_final_result.append("Armor, +2 half plate")
            elif desc_roll >= 6 and desc_roll <= 4:
                treasure_final_result.append("Armor, +2 plate")
            elif desc_roll >= 6 and desc_roll <= 6:
                treasure_final_result.append("Armor, +3 studded leather")
            elif desc_roll >= 6 and desc_roll <= 8:
                treasure_final_result.append("Armor, +3 breastplate")
            elif desc_roll >= 6 and desc_roll <= 10:
                treasure_final_result.append("Armor, +3 splint")
            elif desc_roll == 11:
                treasure_final_result.append("Armor, +3 half plate")
            elif desc_roll == 12:
                treasure_final_result.append("Armor, +3 plate")
        elif roll == 77:
            treasure_final_result.append("Apparatus of Kwalish")
        elif roll == 78:
            treasure_final_result.append("Armor of invulnerability")
        elif roll == 79:
            treasure_final_result.append("Belt of storm giant strength")
        elif roll == 80:
            treasure_final_result.append("Cubic gate")
        elif roll == 81:
            treasure_final_result.append("Deck of many things")
        elif roll == 82:
            treasure_final_result.append("Efreeti chain")
        elif roll == 83:
            treasure_final_result.append("Armor of resistance (half plate)")
        elif roll == 84:
            treasure_final_result.append("Horn of Valhalla (iron)")
        elif roll == 85:
            treasure_final_result.append("Instrument of the bards (OIIamh harp)")
        elif roll == 86:
            treasure_final_result.append("Ioun stone (greater absorption)")
        elif roll == 87:
            treasure_final_result.append("Ioun stone (mastery)")
        elif roll == 88:
            treasure_final_result.append("Ioun stone (regeneration)")
        elif roll == 89:
            treasure_final_result.append("Plate armor of etherealness")
        elif roll == 90:
            treasure_final_result.append("Plate armor of resistance")
        elif roll == 91:
            treasure_final_result.append("Ring of air elemental command")
        elif roll == 92:
            treasure_final_result.append("Ring of earth elemental command")
        elif roll == 93:
            treasure_final_result.append("Ring of fire elemental command")
        elif roll == 94:
            treasure_final_result.append("Ring of three wishes")
        elif roll == 95:
            treasure_final_result.append("Ring of water elemental command")
        elif roll == 96:
            treasure_final_result.append("Sphere of annihilation")
        elif roll == 97:
            treasure_final_result.append("Talisman of pure good")
        elif roll == 98:
            treasure_final_result.append("Talisman of the sphere")
        elif roll == 99:
            treasure_final_result.append("Talisman of ultimate evil")
        elif roll == 100:
            treasure_final_result.append("Tome of the stilled tongue")

def roll_for_gold(num,die,mult,cur):
    treasure_final_result.append(str('{:,}'.format(mult * random.randint(num,num*die))) + cur)

def individual_treasure(x): # x = challenge

    if x >= 0 and x <= 4:
        #print("0-4")
        if roll >= 1 and roll <= 30:
            roll_for_gold(5,6,1,cp)#5d6cp
        elif roll >= 31 and roll <= 60:
            roll_for_gold(4,6,1,sp)#4d6sp
        elif roll >= 61 and roll <= 70:
            roll_for_gold(3,6,1,ep)#3d6ep
        elif roll >= 71 and roll <= 95:
            roll_for_gold(3,6,1,gp)#3d6gp
        elif roll >= 96 and roll <= 100:
            roll_for_gold(1,6,1,pp)#1d6pp
    if x >= 5 and x <= 10:
        #print("5-10")
        if roll >= 1 and roll <= 30:
            roll_for_gold(1,6,10,ep)#1d6*10ep
            roll_for_gold(4,6,100,cp)#4d6*100cp
        elif roll >= 31 and roll <= 60:
            roll_for_gold(2,6,10,gp)#2d6*10gp
            roll_for_gold(6,6,10,sp)#6d6*10sp
        elif roll >= 61 and roll <= 70:
            roll_for_gold(2,6,10,gp)#2d6*10gp
            roll_for_gold(3,6,10,ep)#3d6*10ep
        elif roll >= 71 and roll <= 95:
            roll_for_gold(4,6,10,gp)#4d6*10gp
        elif roll >= 96 and roll <= 100:
            roll_for_gold(3,6,1,pp)#3d6pp
            roll_for_gold(2,6,10,gp)#2d6*10gp
    if x >= 11 and x <= 16:
        #print("11-16")
        if roll >= 1 and roll <= 20:
            roll_for_gold(1,6,100,gp)#1d6*100gp
            roll_for_gold(4,6,100,sp)#4d6*100sp
        elif roll >= 21 and roll <= 35:
            roll_for_gold(1,6,100,gp)#1d6*100gp
            roll_for_gold(1,6,100,ep)#1d6*100ep
        elif roll >= 36 and roll <= 75:
            roll_for_gold(1,6,10,pp)#1d6*10pp
            roll_for_gold(2,6,100,gp)#2d6*100gp
        elif roll >= 76 and roll <= 100:
            roll_for_gold(2,6,10,pp)#2d6*10pp
            roll_for_gold(2,6,100,gp)#2d6*100gp
    if x >= 17:
        #print("17+")
        if roll >= 1 and roll <= 15:
            roll_for_gold(8,6,100,gp)#8d6*100gp
            roll_for_gold(2,6,1000,ep)#2d6*1000ep
        elif roll >= 16 and roll <= 55:
            roll_for_gold(1,6,100,pp)#1d6*100pp
            roll_for_gold(1,6,1000,gp)#1d6*1000gp
        elif roll >= 56 and roll <= 100:
            roll_for_gold(2,6,100,pp)#2d6*100pp
            roll_for_gold(1,6,1000,gp)#1d6*1000gp

def hoard_treasure(x):
    if x >= 0 and x <= 4:
        roll_for_gold(2,6,10,gp)#2d6*10gp
        roll_for_gold(3,6,100,sp)#3d6*100sp
        roll_for_gold(6,6,100,cp)#6d6*100cp

        if roll >= 7 and roll <= 16:
            ten_gp_gemstones(2,6)#2d6
        elif roll >= 17 and roll <= 26:
            twentyfive_gp_art_objects(2,4)#2d4
        elif roll >= 27 and roll <= 36:
            fifty_gp_gemstones(2,6)#2d6
        elif roll >= 37 and roll <= 44:
            ten_gp_gemstones(2,6)#2d6
            magic_item_table_a(1,6)#1d6
        elif roll >= 45 and roll <= 52:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_a(1,6)#1d6
        elif roll >= 53 and roll <= 60:
            fifty_gp_gemstones(2,6)#2d6
            magic_item_table_a(1,6)#1d6
        elif roll >= 61 and roll <= 65:
            ten_gp_gemstones(2,6)#2d6
            magic_item_table_b(1,4)#1d4
        elif roll >= 66 and roll <= 70:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_b(1,4)#1d4
        elif roll >= 71 and roll <= 75:
            fifty_gp_gemstones(2,6)#2d6
            magic_item_table_b(1,4)#1d4
        elif roll >= 76 and roll <= 78:
            ten_gp_gemstones(2,6)#2d6
            magic_item_table_c(1,4)#1d4
        elif roll >= 79 and roll <= 80:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_c(1,4)#1d4
        elif roll >= 81 and roll <= 85:
            fifty_gp_gemstones(2,6)#2d6
            magic_item_table_c(1,4)#1d4
        elif roll >= 86 and roll <= 92:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_f(1,4)#1d4
        elif roll >= 93 and roll <= 97:
            fifty_gp_gemstones(2,6)#2d6
            magic_item_table_f(1,4)#1d4
        elif roll >= 98 and roll <= 99:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_g(1,1)#once
        elif roll == 100:
            fifty_gp_gemstones(2,6)#2d6
            magic_item_table_g(1,1)#once
    elif x >= 5 and x <= 10:
        roll_for_gold(3,6,10,pp)#3d6*10pp
        roll_for_gold(6,6,100,gp)#6d6*100gp
        roll_for_gold(2,6,1000,sp)#2d6*1000sp
        roll_for_gold(2,6,100,cp)#2d6*100cp

        if roll >= 5 and roll <= 10:
            twentyfive_gp_art_objects(2,4)#2d4
        elif roll >= 11 and roll <= 16:
            fifty_gp_gemstones(3,6)#3d6
        elif roll >= 17 and roll <= 22:
            onehundred_gp_gemstones(3,6)#3d6
        elif roll >= 23 and roll <= 28:
            twohundredfifty_gp_art_objects(2,4)#2d4
        elif roll >= 29 and roll <= 32:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_a(1,6)#1d6
        elif roll >= 33 and roll <= 36:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_a(1,6)#1d6
        elif roll >= 37 and roll <= 40:
            onehundred_gp_gemstones(3,6)#3d6
            magic_item_table_a(1,6)#1d6
        elif roll >= 41 and roll <= 44:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_a(1,6)#1d6
        elif roll >= 45 and roll <= 49:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_b(1,4)#1d4
        elif roll >= 50 and roll <= 54:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_b(1,4)#1d4
        elif roll >= 55 and roll <= 59:
            onehundred_gp_gemstones(3,6)#3d6
            magic_item_table_b(1,4)#1d4
        elif roll >= 60 and roll <= 63:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_b(1,4)#1d4
        elif roll >= 64 and roll <= 66:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_c(1,4)#1d4
        elif roll >= 67 and roll <= 69:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_c(1,4)#1d4
        elif roll >= 70 and roll <= 72:
            onehundred_gp_gemstones(3,6)#3d6
            magic_item_table_c(1,4)#1d4
        elif roll >= 73 and roll <= 74:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_c(1,4)#1d4
        elif roll >= 75 and roll <= 76:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_d(1,1)#once
        elif roll >= 77 and roll <= 78:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_d(1,1)#once
        elif roll == 79:
            onehundred_gp_gemstones(3,6)#3d6
            magic_item_table_d(1,1)#once
        elif roll == 80:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_d(1,1)#once
        elif roll >= 81 and roll <= 84:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_f(1,4)#1d4
        elif roll >= 85 and roll <= 88:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_f(1,4)#1d4
        elif roll >= 89 and roll <= 91:
            onehundred_gp_gemstones(3,6)#3d6
            magic_item_table_f(1,4)#1d4
        elif roll >= 92 and roll <= 94:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_f(1,4)#1d4
        elif roll >= 95 and roll <= 96:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_g(1,4)#1d4
        elif roll >= 97 and roll <= 98:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_g(1,4)#1d4
        elif roll == 99:
            twentyfive_gp_art_objects(2,4)#2d4
            magic_item_table_h(1,1)#once
        elif roll == 100:
            fifty_gp_gemstones(3,6)#3d6
            magic_item_table_h(1,1)#once
    elif x >= 11 and x <= 16:
        roll_for_gold(5,6,100,pp)#5d6*100pp
        roll_for_gold(4,6,1000,gp)#4d6*1000gp

        if roll >= 4 and roll <= 6:
            twohundredfifty_gp_art_objects(2,4)#2d4
        elif roll >= 7 and roll <= 9:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
        elif roll >= 10 and roll <= 12:
            fivehundred_gp_gemstones(3,6)#3d6
        elif roll >= 13 and roll <= 15:
            onethousand_gp_gemstones(3,6)#3d6
        elif roll >= 16 and roll <= 19:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_b(1,6)#1d6
            magic_item_table_a(1,4)#1d4
        elif roll >= 20 and roll <= 23:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_b(1,6)#1d6
            magic_item_table_a(1,4)#1d4
        elif roll >= 10 and roll <= 26:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_b(1,6)#1d6
            magic_item_table_a(1,4)#1d4
        elif roll >= 24 and roll <= 29:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_b(1,6)#1d6
            magic_item_table_a(1,4)#1d4
        elif roll >= 30 and roll <= 35:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_c(1,6)#1d6
        elif roll >= 36 and roll <= 40:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_c(1,6)#1d6
        elif roll >= 41 and roll <= 45:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_c(1,6)#1d6
        elif roll >= 46 and roll <= 50:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_c(1,6)#1d6
        elif roll >= 51 and roll <= 54:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_d(1,4)#1d4
        elif roll >= 55 and roll <= 58:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_d(1,4)#1d4
        elif roll >= 59 and roll <= 62:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_d(1,4)#1d4
        elif roll >= 63 and roll <= 66:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_d(1,4)#1d4
        elif roll >= 67 and roll <= 68:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_e(1,1)#once
        elif roll >= 69 and roll <= 70:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_e(1,1)#once
        elif roll >= 71 and roll <= 72:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_e(1,1)#once
        elif roll >= 73 and roll <= 74:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_e(1,1)#once
        elif roll >= 75 and roll <= 76:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_g(1,4)#1d4
            magic_item_table_f(1,1)#once
        elif roll >= 77 and roll <= 78:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_g(1,4)#1d4
            magic_item_table_f(1,1)#once
        elif roll >= 79 and roll <= 80:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_g(1,4)#1d4
            magic_item_table_f(1,1)#once
        elif roll >= 81 and roll <= 82:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_g(1,4)#1d4
            magic_item_table_f(1,1)#once
        elif roll >= 83 and roll <= 85:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_h(1,4)#1d4
        elif roll >= 86 and roll <= 88:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_h(1,4)#1d4
        elif roll >= 89 and roll <= 90:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_h(1,4)#1d4
        elif roll >= 91 and roll <= 92:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_h(1,4)#1d4
        if roll >= 93 and roll <= 94:
            twohundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_i(1,1)#once
        elif roll >= 95 and roll <= 96:
            sevenhundredfifty_gp_art_objects(2,4)#2d4
            magic_item_table_i(1,1)#once
        elif roll >= 97 and roll <= 98:
            fivehundred_gp_gemstones(3,6)#3d6
            magic_item_table_i(1,1)#once
        elif roll >= 99 and roll <= 100:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_i(1,1)#once
    elif x >= 17:
        roll_for_gold(8,6,1000,pp)#8d6*1000pp
        roll_for_gold(12,6,1000,gp)#12d6*1000gp

        if roll >= 3 and roll <= 5:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_c(1,8)#1d8
        elif roll >= 6 and roll <= 8:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_c(1,8)#1d8
        elif roll >= 9 and roll <= 11:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_c(1,8)#1d8
        elif roll >= 12 and roll <= 14:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_c(1,8)#1d8
        elif roll >= 15 and roll <= 22:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_d(1,6)#1d6
        elif roll >= 23 and roll <= 30:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_d(1,6)#1d6
        elif roll >= 31 and roll <= 38:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_d(1,6)#1d6
        elif roll >= 39 and roll <= 46:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_d(1,6)#1d6
        elif roll >= 47 and roll <= 52:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_e(1,6)#1d6
        elif roll >= 53 and roll <= 58:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_e(1,6)#1d6
        elif roll >= 59 and roll <= 63:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_e(1,6)#1d6
        elif roll >= 64 and roll <= 68:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_e(1,6)#1d6
        elif roll == 69:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_g(1,4)#1d4
        elif roll == 70:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_g(1,4)#1d4
        elif roll == 71:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_g(1,4)#1d4
        elif roll == 72:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_g(1,4)#1d4
        elif roll >= 73 and roll <= 74:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_h(1,4)#1d4
        elif roll >= 75 and roll <= 76:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_h(1,4)#1d4
        elif roll >= 77 and roll <= 78:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_h(1,4)#1d4
        elif roll >= 79 and roll <= 80:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_h(1,4)#1d4
        elif roll >= 81 and roll <= 85:
            onethousand_gp_gemstones(3,6)#3d6
            magic_item_table_i(1,4)#1d4
        elif roll >= 86 and roll <= 90:
            twothousandfivehundred_gp_art_objects(1,10)#1d10
            magic_item_table_i(1,4)#1d4
        elif roll >= 91 and roll <= 95:
            seventhousandfivehundred_gp_art_objects(1,4)#1d4
            magic_item_table_i(1,4)#1d4
        elif roll >= 96 and roll <= 100:
            fivethousand_gp_gemstones(1,8)
            magic_item_table_i(1,4)#1d4

#ten_gp_gemstones()
#fifty_gp_gemstones()
#onehundred_gp_gemstones()
#fivehundred_gp_gemstones()
#onethousand_gp_gemstones()
#fivethousand_gp_gemstones()

#twentyfive_gp_art_objects()
#twohundredfifty_gp_art_objects()
#sevenhundredfifty_gp_art_objects()
#twothousandfivehundred_gp_art_objects()
#seventhousandfivehundred_gp_art_objects()

#magic_item_table_a()
#magic_item_table_b()
#magic_item_table_c()
#magic_item_table_d()
#magic_item_table_e()
#magic_item_table_f()
#magic_item_table_g()
#magic_item_table_h()
#magic_item_table_i()

def roll_for_treasure():
    if options.type == "ind":
        individual_treasure(int(options.cr))
    elif options.type == "hoard":
        hoard_treasure(int(options.cr))
    else:
        print("you have supplied an invalid type, either ind or hoard")
        exit(1)

treasure_final_result = []#initialize result array

roll = random.randint(1,100)#roll 1d100
roll_for_treasure()#evaluate roll for treasure

newlist = "\n".join(treasure_final_result)#separate entries with new line
print(newlist.lower())#print everything lowercase

#print("---any weapons---")
#print(random_weapon('any','any','any','any'))

#print("---simple melee---")
#print(random_weapon('simple','melee','any','any'))
#print("---simple ranged---")
#print(random_weapon('simple','ranged','any','any'))
#print("---martial melee---")
#print(random_weapon('martial','melee','any','any'))
#print("---martial ranged---")
#print(random_weapon('martial','ranged','any','any'))

#print("---any sword---")
#print(random_weapon('any','any','sword','any'))
#print("---any axe---")
#print(random_weapon('any','any','axe','any'))
#print("---any axe or sword---")
#print(random_weapon('any','any','axe or sword','any'))
#print("---any piercing---")
#print(random_weapon('any','any','any','piercing'))
#print("---any slashing sword---")
#print(random_weapon('any','any','sword','slashing'))

#print("---any armor---")
#print(random_armor('any'))

#print("---any light---")
#print(random_armor('light'))
#print("---any medium---")
#print(random_armor('medium'))
#print("---any heavy---")
#print(random_armor('heavy'))
#print("---any shield---")
#print(random_armor('shield'))
#print("---any medium or heavy but not hide---")
#x = random_armor('medium or heavy')
#while x == 'Hide':
#    x = random_armor('light or medium')
#print(x)

#print("---any ammo---")
#print(random_ammunition('any','any','any'))
#print("---any simple---")
#print(random_ammunition('simple','any','any'))
#print("---any martial---")
#print(random_ammunition('martial','any','any'))
#print("---any piercing---")
#print(random_ammunition('any','any','piercing'))
#print("---any bludgeoning---")
#print(random_ammunition('any','any','bludgeoning'))
